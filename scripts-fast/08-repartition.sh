#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)

touch ${vars}/status/08-repartition.begin

parted -s -- ${disk} rm 2
#parted -s -a optimal -- /dev/sda resizepart 1 '100%'
printf "d\nn\np\n1\n\n\nn\np\nw\n" | fdisk -B ${disk}
partprobe

touch ${vars}/status/08-repartition.end

