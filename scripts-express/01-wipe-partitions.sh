#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)

touch ${vars}/status/01-wipe-partitions.begin

blkdiscard -s ${disk}
blkdiscard ${disk}

dd if=/dev/zero of=${disk} bs=512 count=1024
dd if=/dev/zero of=${disk} bs=512 seek=$(( $(blockdev --getsz ${disk}) - 1024 )) count=1024

partprobe

lsblk ${disk} > ${vars}/status/01-wipe-partitions.test

touch ${vars}/status/01-wipe-partitions.end
