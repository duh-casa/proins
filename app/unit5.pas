{+-----------------------------------------------------------------------------+}
{| RSO Progressive installer - Unit5 / Form5, version 0.7                      |}
{| (c) Vassugo and Dustwolf, 2020                                              |}
{|                                                                             |}
{| Graphical front-end, application window for viewing this application        |}
{| run-time log. Log is shown in plain-text format, in reverse order, i.e.     |}
{| from last entry to the first entry at the end of view - less scrooling. :)  |}
{+-----------------------------------------------------------------------------+}

unit Unit5;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TForm5 }

  TForm5 = class(TForm)
    Memo1: TMemo;
  private

  public

  end;

var
  Form5: TForm5;

implementation

{$R *.lfm}

//This unit has no logical code to run or to implement, it is used only for
//showing pre-defined elements and contents of run-time log.

end.

{+-----------------------------------------------------------------------------+}
{|                           *** END OF FILE ***                               |}
{+-----------------------------------------------------------------------------+}

