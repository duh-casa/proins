//+-----------------------------------------------------------------------------
//| A simple little Object Pascal program for reading model and serial numbers |
//| off the selected hard drive (selected drive is represented by argument).   |
//| Program can invoke reading from any existing drives, assuming, you specify |
//| the correct and existing absolute drive path as a parameter.               |
//| It works by invonking pre-built *nix binaries with specific arguments to   |
//| concatenate related parameters into end result. This program was created   |
//| for and is being used by Vassugo and DustWolf in project ProIns, however,  |
//| if you find a usefull note over it, go ahead, let there be light. :)       |
//+----------------------------------------------------------------------------+
program GetDiskID(DrivePath);   //The parameter is taken at the run time as a string


{$mode objfpc}{$H+}    //Thou shalt not change this line. Ever.

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}    //Not using extra threads, but must be
  cthreads,                            //loaded for proper syntax-handling.
  {$ENDIF}{$ENDIF}
  Classes, SysUtils, Process;
  //We only need one thread for this job and no special classes to load.
  //Since the job is pretty much straight-forward, let's keep it clean and simple.

var
  //Since we are using terminal envoiroment, all varables must be declared staticly,
  //and no variables should be left dinamicly linked.
  DiskModel  : String;
  DiskSerial : String;
  DrivePath : String;
  Output : String;
  Job : TProcess;
  ShellOutput : TStringList;
  LinePosition : Integer;


procedure GetDiskID();
begin
   LinePosition := 0;   //In te algorithm, we only need one value, and we set it here.
   DrivePath := paramStr(1);    //Te first parameter of the program is actually the drive path itself (hopefully).
   Job := TProcess.Create(nil);
   ShellOutput := TStringList.Create();
   Job.Options := Job.Options + [poWaitOnExit, poUsePipes];
   Job.CommandLine := ('lsblk --nodeps -no model ' + DrivePath);   //Execute the "get model" routine.
   Job.Execute();
   ShellOutput.LoadFromStream(Job.Output);
   DiskModel:= (ShellOutput.Strings[LinePosition]);
   Job.Free();                  //After we get the disk model, we repeat pretty much same
   ShellOutput.Free();          //thing for the serial number, only this time saving it to different variable.
   Job := TProcess.Create(nil);
   ShellOutput := TStringList.Create();
   Job.Options := Job.Options + [poWaitOnExit, poUsePipes];
   Job.CommandLine := ('lsblk --nodeps -no serial '+ DrivePath);    //Execute the "get serial" routine.
   Job.Execute();
   ShellOutput.LoadFromStream(Job.Output);
   DiskSerial:= (ShellOutput.Strings[LinePosition]);
   Job.Free();
   ShellOutput.Free();
   Output := DiskModel + '_' + DiskSerial;   //As defined, result is disk model and disk serial, with "_" symbol in the middle.
   WriteLn(Output);   //Output is a string, represented by "model-number"_"serial-number" combination.
end;

procedure GiveMeHelp();  //Simple procedure that writes out the help section of a program.
begin
   WriteLn('');
   WriteLn('A simple little program for reading model and serial numbers of the');
   WriteLn('attached block devices in the system. A full and absolute path to');
   WriteLn('target drive must be given in form of a first parameter.');
   WriteLn('Output is a single string in form of DEVICE-MODEL_DEVICE-SERIAL-NUMBER');
   WriteLn('and can be used / piped into other Unix-like programs and scripts.');
   WriteLn('');
   WriteLn('Usage:   GetDiskID [PATH_TO_DRIVE]     |   Example: GetDiskID /dev/sda');
   WriteLn('         GetDIskID [ARGUMENTS]         |   Example: GetDiskID -h');
   WriteLn('');
   WriteLn('The -h or --help parameter has obviosly brought you here so no explanation');
   WriteLn('needed for that. Since the program is really short and simple, ');
   WriteLn('so should be the help section. But one thing you should know:');
   WriteLn('This program should be run under sudo mode, or else...');
   WriteLn('');
   WriteLn('Exit status codes:');
   WriteLn('0: Everything is fine, program completed correctly.');
   WriteLn('1: Missing or inadequate arguments');
   WriteLn('2: Access violation (probably non existing drive path)');
   WriteLn('3: Authority level violation (non-sudo run attempt)');
   WriteLn('4: Unsuspected, user related, but probably serious (user) problems');
   WriteLn('');
   WriteLn('');
end;

begin
  if (paramStr(1) = '') then   //If there is no parameters, the exception is raised.
  begin
    WriteLn('Not enough arguments. Missing a drive path (ex. "/dev/sda").');
    Halt(1);  //Exception 1 is logged.
  end;
  if (paramStr(1) = '-h') then   //Calling for help.
  begin
    GiveMeHelp();
    Halt(0); //Everything went out fine and dandy, so no exception is logged.
  end;
  if (paramStr(1) = '--h') then    //Calling for help.
  begin
    GiveMeHelp();
    Halt(0); //Everything went out fine and dandy, so no exception is logged.
  end;
  if (paramStr(1) = '-help') then  //Calling for help.
  begin
    GiveMeHelp();
    Halt(0); //Everything went out fine and dandy, so no exception is logged.
  end;
  if (paramStr(1) = '--help') then  //Calling for help.  There are four if's for same thing here, i noticed that too,
  begin                             //and it's not idiot programming, but FPC limitations. :)
    GiveMeHelp();
    Halt(0); //Everything went out fine and dandy, so no exception is logged.
  end;
  if (paramStr(1).Chars[0] = '/') then  //If there is a argument, we check if it is properly formatted.
  begin
  if (FileExists(paramStr(1))) then     //If the path actually exists, we run the directive
  begin
     GetDiskID();
     Halt(0);  //Everything went out fine and dandy, so no exception is logged.
  end
  else
     WriteLn('The specified block device does not exist. Typo?');   //Otherwise we encourage user to rethink.
     Halt(2);    //If program tries to access non-existing path, exception 2 is logged.
  end;
  if (paramStr(1).Chars[0] <> '/') then    //Now this one is simply a easter-egg. :)
  begin
     WriteLn('Well, what did you expect? A bunny rabbit!?');
     WriteLn('Simon says: Garbage in, garbage out!');
     WriteLn();
     WriteLn();
     Halt(4);  //And exception is logged anyway.
  end;
  Halt(3);  //If nothing of above is adequate, something must went wrong and we log exception.
end.



//+----------------------------END OF FILE-------------------------------------+
