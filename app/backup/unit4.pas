{+-----------------------------------------------------------------------------+}
{| RSO Progressive installer - Unit4 / Form4, version 0.7                      |}
{| (c) Vassugo and Dustwolf, 2020                                              |}
{|                                                                             |}
{| Graphical front-end, application window for confirming user defined         |}
{| selection of install parameters. This form also "switches" between select   |}
{| and automation part of installer application.                               |}
{+-----------------------------------------------------------------------------+}


unit Unit4;

{$mode objfpc}{$H+}

interface

// Important: Overload calls must be implemented in each unit - for each used unit
uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons, ExtCtrls, Process,
  LCLType, Unix, Unit1;

type

  { TForm4 }

  TForm4 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private

  public
  TargetDir : String;
  //Globally asigned variable for handling bash-command-line output.
  end;

var
  Form4: TForm4;

implementation
uses Unit1;
{$R *.lfm}

{ TForm4 }



//Logging procedure used throughout the application, writing timestamps and
//application relevant run-time data to single, pre-defined plain text file.
procedure AddToLog(LogEntry : string);
var
TimeStamp : string;
begin

   TimeStamp := FormatDateTime('DD. MM. YYYY - hh:mm:ss', now);
   fpsystem('echo ' + TimeStamp + ' - ' + LogEntry + ' >> ' + Form4.TargetDir + '/proins_runlog.log');
end;

//Procedure for checking the actual state, in which installer runs. If parameters were allready set, then it disables
//controls in upper part of program (enabled by default), and leaves set parameters in assigned controls and automaticly
//calls for MainInstaller method to continiue the work since last the restart (or other interruption).
//The procedure also enables the "lower" portion of the window, so user can monitor the progress of instalation.
procedure CheckProgressState();
var
   DumbArray: TStringList;
begin
   If (FileExists(Form1.RunDir + '/vars/user_selected_params.list')) then
   begin
       DumbArray := TStringList.Create();
       DumbArray.LoadFromFile(RunDir + '/vars/user_selected_params.list');
       Form1.ComboBox1.Text := DumbArray.Strings[0];
       Form1.ComboBox2.Text := DumbArray.Strings[1];
       DumbArray.Free();
       Form1.GroupBox1.Enabled := False;
       Form1.GroupBox2.Enabled := False;
       Form1.ComboBox1.Enabled := False;
       Form1.ComboBox2.Enabled := False;
       Form1.BitBtn1.Enabled := False;
       //Upper code is for the "upper" portion of the main window. Below we take care for the "lower" portion.
       Form1.GroupBox3.Enabled := True;
       Form1.GroupBox4.Enabled := True;
       Form1.Label1.Enabled := True;
       Form1.Label2.Enabled := True;
       Form1.ProgressBar1.Enabled := True;
       Form1.ProgressBar2.Enabled := True;
   end;
end;




//Since the end-result of this installer can be quite "abrasive" if user sets wrong array of
//parameters, it is important to protect user from himself. :) Hence the "double-question-routine". :)
procedure FinalQuestion;
var
  Reply, BoxStyle : Integer;
begin
  //User confirmation is implemented trough simple YES/NO infobox, generated directly trough GTK library.
  //By "catching" user responce, progress is either confirmed or aborted.
  BoxStyle := MB_ICONQUESTION + MB_YESNO;
  Reply := Application.MessageBox('Ali ste prepričani, da želite nadaljevati?', 'Namestilnik je pripravljen na začetek dela', BoxStyle);
  if Reply = IDYES then
  begin
    Application.MessageBox('Namestitev je zagnana. Lahko nadaljujete z delom.', 'Namestilnik je pričel z nameščanjem sistema',MB_ICONINFORMATION);
    //Result of user choice is logged in either case.
    AddToLog('[USER-INFO]-Uporabnik dokončno avtoriziral parametre, zaganjam namestilnik z izbranimi parametri.');
    //Writing user choices into specific files, needed for scripts execution.
    fpsystem('echo '+ Form1.ComboBox1.Items[Form1.ComboBox1.ItemIndex] + ' > ' + Form4.TargetDir + '/vars/user_selected_params.list');
    fpsystem('echo '+ Form1.ComboBox2.Items[Form1.ComboBox2.ItemIndex] + ' >> '+ Form4.TargetDir + '/vars/user_selected_params.list');
    AddToLog('[APP-OK]-Nova konfiguracijska datoteka uspešno zapisana na svoje mesto.');
    //Writing disk and instalation choices into apropriate files for scripts to use.
    AddToLog('[APP-EXEC]-Zapisujem izbrani disk v datoteko disk.txt...');
    fpsystem('echo '+ Form1.ComboBox1.Items[Form1.ComboBox1.ItemIndex] + ' > ' + Form4.TargetDir + '/vars/disk.txt');
    AddToLog('[APP-OK]-Datoteka disk.txt uspešno generirana.');
    AddToLog('[APP-EXEC]-Zapisujem izbrano instalacijo v datoteko instal.txt...');
    fpsystem('echo '+ Form1.ComboBox2.Items[Form1.ComboBox2.ItemIndex] + ' > '+ Form4.TargetDir + '/vars/instal.txt');
    AddToLog('[APP-OK]-Datoteka instal.txt uspešno generirana.');
    AddToLog('[APP-EXEC]-Pripravljam zagon glavne komponente namestilnika.');
    //Calling main instalation method.
    Form4.Close();
    Application.Minimize;
    CheckProgressState();
  end
    else
    begin
      Application.MessageBox('Prosimo, ponovno preverite vse parametre, preden potrdite izbiro!', 'Preklic delovanja namestilnika', MB_ICONHAND);
      //Result of user choice is logged in either case.
      AddToLog('[INFO]-Uporabnik preklical dokončno avtorizacijo. Ponovna izbira parametrov.');
      //If user does not confirm selected parameters, the confirmation window closes to prompt user ro re-check parameters again.
      Form4.Close();
      AddToLog('[USER-FAULT]-Zapiram potrditveno okno...');
    end;
end;


//In case of errata before confirmation, the confirm windows closes without parameter confirmation.
procedure TForm4.BitBtn2Click(Sender: TObject);
begin
   AddToLog('[USER-FAULT]]Uporabnik prekinil potrditev namestitve. Ponovna izbira parametrov.');
   Form4.Close();
end;





//If user has checked and double-checked selection of install parameters, the "double-question-confirm" method is invoked.
procedure TForm4.BitBtn1Click(Sender: TObject);
begin
  AddToLog('[APP-INFO]-Uporabnik overil izbrane parametre za nadaljevanje namestitve, pripravljam končno overitev...');
  FinalQuestion();
end;


end.

{+-----------------------------------------------------------------------------+}
{|                           *** END OF FILE ***                               |}
{+-----------------------------------------------------------------------------+}

