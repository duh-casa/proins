#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)

touch ${vars}/status/07-zerowipe.begin

ddrescue --force /dev/zero ${disk}2 ${vars}/erase.log ||:

touch ${vars}/status/07-zerowipe.end
