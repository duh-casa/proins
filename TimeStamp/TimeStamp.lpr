//+-----------------------------------------------------------------------------
//| A simple little Object Pascal program, used in project ProIns as a date    |
//| and time (so called "DateTimeStamp") injector for status files, generated  |
//| by different progressive installer bash scripts. The program allocates     |
//| files such as "01-wipe-partitions.begin" and "01-wipe-partitions.end"      |
//| and "injects" properly formatted date and time, in specific format, used   |
//| in front-end portion of progressive installer application. The first arg.  |
//| in command line is the name of the *.begin or *.end file.                  |
//| TimeStamp is being used by Vassugo and DustWolf in project ProIns, however,|
//| if you find a usefull note over it, go ahead, let there be light. :)       |
//+----------------------------------------------------------------------------+


program TimeStamp;

{$mode objfpc}{$H+}    //Stack-call for Bash script language in Unix-like systems.

uses
    Classes, SysUtils, Unix;

{$IFDEF UNIX}{$IFDEF UseCThreads}    //Not using extra threads, but must be
  cthreads,                          //loaded for proper syntax-handling.
{$ENDIF}{$ENDIF}

var
  DateTimeString : String;   //Global variable for generating properly formatted date and time string.
  FileName : String;         //Global variable for casting target file name to write procedure.


//Simple procedure that writes out the help section of a program.
procedure GiveMeHelp();
begin
   WriteLn('');
   WriteLn('A simple little program used with "training-wheels" scripts for');
   WriteLn('generating and injecting "timestamps" in status files, generated ');
   WriteLn('by each bash script, part of progressive install process.');
   WriteLn('First argument of program is the actual filename, and generated.');
   WriteLn('timestamp is properly formatted and written in the specified file.');
   WriteLn('Note that FILENAME argument can also be an absolute path to target file.');
   WriteLn('');
   WriteLn('Usage:  TimeStamp [FILENAME]   |   Example: TimeStamp 01-wipe-partitions.end');
   WriteLn('        TimeEater [ARGUMENT]   |   Example: TimeEater -h');
   WriteLn('');
   WriteLn('The -h or --help parameter has obviosly brought you here so no additional');
   WriteLn('explaining is needed for that. There really is nothing sophisticated to be,');
   WriteLn('documented about this program.');
   WriteLn('');
   WriteLn('Exit status codes:');
   WriteLn('0: Everything is fine, program completed correctly.');
   WriteLn('1: Missing or inadequate arguments');
   WriteLn('2: Access violation, cannot generate output file. Write permissions?');
   WriteLn('');
   WriteLn('🄯 Author: Vassugo for ProIns projetct, 2020');
end;

//Actual procedure for injecting properly formatted Date and Time into specified target file.
//Note: Since there is no verification of correct target file, file will be generated and/or
//over-written, so be carefull when calling arguments, i.e. calling this program to generate
//timestamp into "wrong" file. The program will force the function and does not either
//care or check for any "collateral damage".
procedure MakeTimeStamp();
begin
   DateTimeString := FormatDateTime('DD. MM. YYYY - hh:mm:ss', now);
   fpsystem('echo ' + DateTimeString + ' >> ' + FileName);
   //Jump back to main program.
end;


begin
  if (paramStr(1) = '') then   //If there are no parameters, the exception is raised.
  begin
    WriteLn('Not enough arguments. Missing target file name. (e.g. 01-partitions-wipe.begin)');
    WriteLn('');
    Halt(1);  //Exception 1 is logged.
  end;
  if (paramStr(1) = '-h') then   //Calling for help.
  begin
    GiveMeHelp();
    Halt(0); //Normal stop, so no exception is logged.
  end;
  if (paramStr(1) = '--h') then    //Calling for help.
  begin
    GiveMeHelp();
    Halt(0); //Normal stop, so no exception is logged.
  end;
  if (paramStr(1) = '-help') then  //Calling for help.
  begin
    GiveMeHelp();
    Halt(0); //Normal stop, so no exception is logged.
  end;
  if (paramStr(1) = '--help') then  //Calling for help.  There are four if's for same thing here, i noticed that too,
  begin                             //and it's not idiot programming, but Object Pascal compiler limitations. :)
    GiveMeHelp();
    Halt(0); //Normal stop, so no exception is logged.
  end;

  //Finally, if the provided argument is not "empty", we try to write timestamp to the file
  if not (paramStr(1) = '') then
  begin
     FileName := (paramStr(1));    //First, we need to cast parameter to global variable for MakeTimeStamp procedure.
     MakeTimeStamp();
     Halt(0);
  end;

  Halt(3);  //If nothing of above is adequate, something must went horribly wrong and we log exception for debugging.
end.

end.

