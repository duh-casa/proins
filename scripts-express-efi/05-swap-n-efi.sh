#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
did=$(../GetDiskID/GetDiskID $2)
vars=../${did}/vars
mkdir -p ${vars}/status
efipart=$(cat ${vars}/efipart.txt)
syspart=$(cat ${vars}/syspart.txt)

touch ${vars}/status/05-swap.begin

mkdir -p /mnt/${did}
mount ${syspart} /mnt/${did}

echo "${efipart}      /boot/efi       vfat    umask=0077      0       1">>/mnt/${did}/etc/fstab

sed -e "/swap/ s/^#*/#/" -i /mnt/${did}/etc/fstab

fallocate -l 4G /mnt/${did}/swapfile
chmod 600 /mnt/${did}/swapfile
mkswap /mnt/${did}/swapfile
echo "/swapfile      none            swap    sw              0       0">>/mnt/${did}/etc/fstab

sync
ls -lah /mnt/${did}/swapfile > ${vars}/status/05-swap.test
cat /mnt/${did}/etc/fstab >> ${vars}/status/05-swap.test

umount /mnt/${did}

touch ${vars}/status/05-swap.end

