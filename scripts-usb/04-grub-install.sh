#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
did=$(../GetDiskID/GetDiskID $2)
vars=../${did}/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)
old_uuid=75afba19-4c72-4985-a32d-ef208ddd74aa
new_uuid=$(lsblk -no UUID ${disk}1)

touch ${vars}/status/04-grub-install.begin

mkdir -p /mnt/${did}
mount ${disk}1 /mnt/${did}

grub-install --recheck --no-floppy --boot-directory=/mnt/${did}/boot ${disk}
sed -i "s/${old_uuid}/${new_uuid}/g" /mnt/${did}/boot/grub/grub.cfg

sync

dd bs=512 count=1 if=${disk} 2>/dev/null | strings > ${vars}/status/04-grub-install.test

umount /mnt/${did}

touch ${vars}/status/04-grub-install.end
