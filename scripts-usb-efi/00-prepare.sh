#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Usage: ./00-prepare.sh authtoken /dev/sda"
  exit 1
fi

vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}
echo $2 > ${vars}/disk.txt
