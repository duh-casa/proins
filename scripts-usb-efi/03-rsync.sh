#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
did=$(../GetDiskID/GetDiskID $2)
vars=../${did}/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)

touch ${vars}/status/03-rsync.begin

mkdir -p /mnt/${did}

mount ${disk}2 /mnt/${did}
mkdir -p /mnt/${did}/boot/efi
mount ${disk}1 /mnt/${did}/boot/efi

rsync -axAXH --info=progress2 /boot/ /mnt/${did}/boot/
rsync -axAXH --info=progress2 /boot/efi/ /mnt/${did}/boot/efi/
sync

ls -lah /mnt/${did} > ${vars}/status/03-rsync.test

umount /mnt/${did}/boot/efi
umount /mnt/${did}

touch ${vars}/status/03-rsync.end
