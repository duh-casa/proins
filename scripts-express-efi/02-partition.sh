#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)

touch ${vars}/status/02-partition.begin

sleep 1

parted -s -a optimal -- ${disk} \
	mklabel msdos \
	mkpart primary fat32 '0%' '512MB' \
	set 1 esp on \
	mkpart primary ext4 '512MB' '100%'

partprobe

sleep 1

efipart=$(lsblk ${disk} -o name -r -n | tail -n 2 | head -n 1)
echo "/dev/${efipart}" > ${vars}/efipart.txt
syspart=$(lsblk ${disk} -o name -r -n | tail -n 1)
echo "/dev/${syspart}" > ${vars}/syspart.txt

efipart=$(cat ${vars}/efipart.txt)
syspart=$(cat ${vars}/syspart.txt)

diskMajor=$(stat -c '%t' ${disk})
diskMinor=$(stat -c '%T' ${disk})
let "diskMinor+=1"
mknod ${efipart} b ${diskMajor} ${diskMinor}

let "diskMinor+=1"
mknod ${syspart} b ${diskMajor} ${diskMinor}

dd if=/dev/zero of=${efipart} bs=512 count=1024
dd if=/dev/zero of=${syspart} bs=512 count=1024

mkfs.fat -F32 ${efipart}
mkfs.ext4 ${syspart}

partprobe

lsblk ${disk} > ${vars}/status/02-partition.test
fsck -N ${efipart} >> ${vars}/status/02-partition.test
fsck -N ${syspart} >> ${vars}/status/02-partition.test

touch ${vars}/status/02-partition.end

