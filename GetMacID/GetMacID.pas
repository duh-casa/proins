//+-----------------------------------------------------------------------------
//| A simple little Object Pascal program for reading MAC adress of active     |
//| network connection. Program has no input arguments and outputs only one    |
//| string with MAC adress, but with degraded formatting. MAC adress is        |
//| given only as a 12-alfanumeric character string without ":" separator.     |
//| Program will create (and remove) a small text file in the process itself   |
//| This program was created for and is being used by Vassugo and              |
//| DustWolf in project ProIns, however,                                       |
//| if you find a usefull note over it, go ahead, let there be light. :)       |
//| 🄯 Author: Vassugo for ProIns projetct, 2020')                              |
//+----------------------------------------------------------------------------+
program GetMacID;


{$mode objfpc}{$H+}    //Thou shalt not change this line. Ever.

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}    //Not using extra threads, but must be
  cthreads,                            //loaded for proper syntax-handling.
  {$ENDIF}{$ENDIF}
  SysUtils, Unix, StrUtils;
  //We only need one thread for this job and one strings-utilities class to help us properly format the end result.
  //Other than mentioned above, the job is pretty much straight-forward, so let's keep it clean and simple.

var
  //Since we are using terminal envoiroment, all varables must be declared staticly,
  //and no variables should be left dinamicly linked. The program is so small, we can
  //handle everything with just three simple variables.

  Output : String;
  MACFile : text;
  List : array of String;

//The actual procedure, that does all the work is "seperated" from main code for obvious security reasons.
procedure GetMacID();
begin
   //Executing actual terminal code to generate file with MAC adresses of all interfaces except "lo". The first adress is the active one.
   fpsystem('find /sys/class/net -mindepth 1 -maxdepth 1 ! -name lo -printf "%P: " -execdir cat {}/address \; > MAC.adresses.found');
   //The file is generated as a temporary pool for easier manipulation of found MAC adresses.
   AssignFile(MACFile,'MAC.adresses.found');    //The generated file is asigned to a variable for further processing.
   Reset(MACFile);      //We have to reset the index counter if we want to steer away from raising all kind of system exceptions and hard-errors.
   Try
      While Not Eof(MACFile) Do        //We load each line separatley into prepared array of strings, until the file hits EOF.
      begin
         SetLength(List, Length(List) + 1);   //With each pass, we expand the array and reserve memory space for next character.
         ReadLn(MACFile, List[High(List)]);   //Just before we repeat the step, we store te line at that position into array.
      end;
   Finally
     CloseFile(MACFile);   // After all the lines are loaded into array, the file is of no use for us anymore, so we release it form stack.
   end;
   //The file "MAC.adresses.found consists of ALL (active and un-active) MAC adresses, except the MAC adress of the "lo" interface.
   //The first entry in this file, i.e. the first line will allways be the primary active network connection. If no connection is
   //established at the moment of running the program, the first so called "wired" interface will be listed.

   //Post-processing of first entry (the first entry is assumed to be the active one) is done in last five lines of this segment of code.
   Output := List[0];                 //We load a single string from a StringList in position zero.
   Output := (RightStr(Output, 17));  //In this step, we "cut" the last 17 characters from the right side of given string from previous step.
   Output := (UpperCase(Output));     //Now the Output string consist only of the MAC adress itself, so we convert it to ALL-UPPERCASE.
   Output := (DelChars(Output, ':')); //The last step is to get rid of colons in the string, ba using DelChars method over string itself.
   WriteLn(Output);  //Finally, we can return the result in form of a simple string: MAC adress, all chars in UPPERCASE, no colons.
   fpsystem('rm -rf MAC.adresses.found');  //Since we allready returned result, let's keep it clean and dispose of files not needed anymore.
end;


begin
   GetMacID();  //Call the actual procedure to munch out the MAC adress in it's special formatting.
   Halt(0);     //And we safely stop the programm. Well done.
end.

//+----------------------------END OF FILE-------------------------------------+
