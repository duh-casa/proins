#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
diskID=$(../GetDiskID/GetDiskID $2)

tar -zcf ../${diskID}.tar.gz ../${diskID}
curl -F "data=@../${diskID}.tar.gz" -F "filename=${diskID}.tar.gz" http://proins
