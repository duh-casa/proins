{+-----------------------------------------------------------------------------+}
{| RSO Progressive installer - Unit4 / Form4, version 0.7                      |}
{| (c) Vassugo and Dustwolf, 2020                                              |}
{|                                                                             |}
{| Graphical front-end, application window for confirming user defined         |}
{| selection of install parameters. This form also "switches" between select   |}
{| and automation port of installer application.                               |}
{+-----------------------------------------------------------------------------+}


unit Unit4;

{$mode objfpc}{$H+}

interface

// Important: Overload calls must be implemented in each unit - for each used unit
uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons, ExtCtrls, Process,
  LCLType;

type

  { TForm4 }

  TForm4 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private

  public

  end;

var
  Form4: TForm4;

implementation
uses Unit1;
{$R *.lfm}

{ TForm4 }

//Globally asigned variable for handling bash-command-line output.
var
   s : ansistring;


//Logging procedure used throughout the application, writing timestamps and
//application relevant run-time data to single, pre-defined plain text file.
procedure AddToLog(LogEntry : string);
var
   TimeStamp : string;
begin
   TimeStamp := FormatDateTime('DD-MM-YYYY @hh:mm:ss', now);
   RunCommand('/bin/bash',['-c', 'echo ' + TimeStamp + ' - ' + LogEntry + ' >> proins_runlog.log'], s);
end;


//Since the end-result of this installer can be quite "abrasive" if user sets wrong array of
//parameters, it is important to protect user from himself. :) Hence the "double-question-routine". :)
procedure FinalQuestion;
var
  Reply, BoxStyle : Integer;
begin
  //User confirmation is implemented trough simple YES/NO infobox, generated directly trough GTK library.
  //By "catching" user responce, progress is either confirmed or aborted.
  BoxStyle := MB_ICONQUESTION + MB_YESNO;
  Reply := Application.MessageBox('Ali ste prepričani, da želite nadaljevati?', 'Namestilnik je pripravljen na začetek dela', BoxStyle);
  if Reply = IDYES then
  begin
    Application.MessageBox('Namestitev je zagnana. Lahko nadaljujete z delom.', 'Namestilnik je pričel z nameščanjem sistema',MB_ICONINFORMATION);
    //Result of user choice is logged in either case.
    AddToLog('[USER-INFO]-Uporabnik dokončno avtoriziral parametre, zaganjam namestilnik z izbranimi parametri.');
    RunCommand('/bin/bash',['-c','echo '+ Form1.ComboBox1.Items[Form1.ComboBox1.ItemIndex] + ' > /proins/vars/user_selection.log'], s);
    RunCommand('/bin/bash',['-c','echo '+ Form1.ComboBox2.Items[Form1.ComboBox2.ItemIndex] + ' >> /proins/vars/user_selection.log'], s);
    AddToLog('[APP-OK]-Nova konfiguracijska datoteka uspešno zapisana na svoje mesto.');
    AddToLog('[APP-EXEC]-Pripravljam zagon glavne komponente namestilnika.');
    Form1.Button1.Click();
    Form4.Close();
  end
    else
    begin
      Application.MessageBox('Prosimo, ponovno preverite vse parametre, preden potrdite izbiro!', 'Preklic delovanja namestilnika', MB_ICONHAND);
      //Result of user choice is logged in either case.
      AddToLog('[INFO]-Uporabnik preklical dokončno avtorizacijo. Ponovna izbira parametrov.');
      //If user does not confirm selected parameters, the confirmation window closes to prompt user ro re-check parameters again.
      Form4.Close();
      AddToLog('[USER-FAULT]-Zapiram potrditveno okno...');
    end;
end;


//In case of errata before confirmation, the confirm windows closes without parameter confirmation.
procedure TForm4.BitBtn2Click(Sender: TObject);
begin
   AddToLog('[FAULT]]Uporabnik prekinil potrditev namestitve. Ponovna izbira parametrov.');
   Form4.Close();
end;

//If user has checked and double-checked selection of install parameters, the "double-question-confirm" method is invoked.
procedure TForm4.BitBtn1Click(Sender: TObject);
begin
  AddToLog('[APP-INFO]-Uporabnik overil izbrane parametre za nadaljevanje namestitve, pripravljam končno overitev...');
  FinalQuestion();
end;


end.

{+-----------------------------------------------------------------------------+}
{|                           *** END OF FILE ***                               |}
{+-----------------------------------------------------------------------------+}

