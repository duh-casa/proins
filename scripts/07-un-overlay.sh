#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status

touch ${vars}/status/07-un-overlay.begin

mount -o remount,rw /rofs
rsync -axAX --info=progress2 --delete / /rofs

rm /etc/initramfs-tools/hooks/overlay
rm /etc/initramfs-tools/scripts/init-bottom/overlay
update-initramfs -u

rsync -axAX --info=progress2 --delete /boot/ /rofs/boot
rsync -axAX --info=progress2 --delete /etc/initramfs-tools/ /rofs/etc/initramfs-tools

touch ${vars}/status/07-un-overlay.end

