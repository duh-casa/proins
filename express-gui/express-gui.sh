#!/bin/bash
DIALOG=${DIALOG=dialog}
USERKEY=${USERKEY}
DISKTEXT=${DISKTEXT}

function jumpto
{
    label=$1
    cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
    eval "$cmd"
    exit
}

$DIALOG --backtitle "RSO ProIns 0.9 - Namestitveni program RSO za FerenOS"\
        --title "Zagon namestitvenega programa" --clear \
        --yesno "Ta program bo namestil FerenOS na disk v računalniku. Pred nadaljevanjem se prepričajte, da ste odstranili zagonski USB ključ oziroma zagonski DVD! Ali lahko nadaljujem? " 10 70

case $? in
  0)
    rm /tmp/local_disks.txt
    echo "V tem računalniku so zaznani naslednji diskovni pogoni:" >> /tmp/local_disks.txt
    echo " " >> /tmp/local_disks.txt
    lsblk -o NAME,SIZE,VENDOR,MODEL | grep -v loop >> /tmp/local_disks.txt
    echo " " >> /tmp/local_disks.txt
    echo " " >> /tmp/local_disks.txt
    echo "V naslednjem koraku se boste morali odločiti," >> /tmp/local_disks.txt 
    echo "na katerega od prikazanih pogonov boste namestili sistem." >> /tmp/local_disks.txt
    dialog --backtitle "Rso ProIns 0.9 - Namestitveni program RSO za FerenOS" \
           --title "Pregled lokalnih diskovnih pogonov" --clear \
           --textbox /tmp/local_disks.txt 17 63 
    rm /tmp/local_disks.txt
    jumpto DiskSelection
    ;;
  1)
    clear
    $DIALOG --backtitle "RSO ProIns 0.9 - Namestitveni program RSO za FerenOS"\
            --title "Prekinitev namestitvenega programa" --clear\
            --msgbox "Uporabnik je preklical zagon namestitve" 5 50
            clear
    ;;
  255)
    clear
    echo "Zaznan ukaz ESC, prekinjam izvajanje namestitve"
    ;;
esac

DiskSelection:
$DIALOG --backtitle "RSO ProINs 0.9 - Namestitveni program RSO za FerenOS"\
        --title "Izbira ciljnega diskovnega pogona" --clear\
        --inputbox "Vnesite absolutno pot (npr. /dev/sda ali /dev/sdb) do ciljnega diska, na katerega želite namestiti sistem:" 12 60 2>DISKTEXT
case $? in
  0)
    clear
    dialog  --backtitle "RSO ProIns 0.9 - Namestitveni program RSO za FerenOS"\
            --title "Potrditev namestitve na izbrani diskovni pogon" --clear\
            --yesno "Izbrani pogon je $DISKTEXT, ali je to pravilno in se namestitev lahko začne?" 10 50 
            
    ;;
  1)
    rm /tmp/local_disks.txt
    echo "V tem računalniku so zaznani naslednji diskovni pogoni:" >> /tmp/local_disks.txt
    echo " " >> /tmp/local_disks.txt
    lsblk -o NAME,SIZE,VENDOR,MODEL | grep -v loop >> /tmp/local_disks.txt
    echo " " >> /tmp/local_disks.txt
    echo " " >> /tmp/local_disks.txt
    echo "V naslednjem koraku se boste morali odločiti," >> /tmp/local_disks.txt 
    echo "na katerega od prikazanih pogonov boste namestili sistem." >> /tmp/local_disks.txt
    dialog --backtitle "Rso ProIns 0.9 - Namestitveni program RSO za FerenOS" \
           --title "Pregled lokalnih diskovnih pogonov" --clear \
           --textbox /tmp/local_disks.txt 17 63 
    rm /tmp/local_disks.txt
    jumpto DiskSelection
    ;;     
esac
