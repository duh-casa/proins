{+-----------------------------------------------------------------------------+}
{| RSO Progressive installer - Unit3 / Form3, version 0.7                      |}
{| (c) Vassugo and Dustwolf, 2020                                              |}
{|                                                                             |}
{| Graphical front-end, application window for listing dates and times of      |}
{| run-instances of this application. Serves as a simple "monitor" to view     |}
{| the progress of each step, as the application sequentially calls each step. |}
{+-----------------------------------------------------------------------------+}


unit Unit3;

{$mode objfpc}{$H+}

interface

// Important: Overload calls must be implemented in each unit - for each used unit
uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons;

type

  { TForm3 }

  TForm3 = class(TForm)
    BitBtn1: TBitBtn;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    procedure BitBtn1Click(Sender: TObject);
  private

  public

  end;

var
  Form3: TForm3;

implementation

{$R *.lfm}

{ TForm3 }


//Since there is no code to drive this section of application, only procedure
//implemented in this form is window-close method. Since this window is used
//more than one time in application runtime, no destructor is implemented.
procedure TForm3.BitBtn1Click(Sender: TObject);
begin
  Form3.Close();
end;

end.

{+-----------------------------------------------------------------------------+}
{|                           *** END OF FILE ***                               |}
{+-----------------------------------------------------------------------------+}

