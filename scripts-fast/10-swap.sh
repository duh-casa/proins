#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status

touch ${vars}/status/10-swap.begin

fallocate -l 4G /swapfile
chmod 600 /swapfile
mkswap /swapfile
echo "/swapfile      none            swap    sw              0       0">>/etc/fstab
swapon -a

touch ${vars}/status/10-swap.end

