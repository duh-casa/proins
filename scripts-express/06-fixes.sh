#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
did=$(../GetDiskID/GetDiskID $2)
vars=../${did}/vars
mkdir -p ${vars}/status
syspart=$(cat ${vars}/syspart.txt)

touch ${vars}/status/06-fixes.begin

mkdir -p /mnt/${did}
mount ${syspart} /mnt/${did}

if [ -f /mnt/${did}/home/fixes.sh ]; then
 /mnt/${did}/home/fixes.sh authtoken /mnt/${did} > ${vars}/status/06-fixes.test
fi

fstrim /mnt/${did}

sync
umount /mnt/${did}

touch ${vars}/status/06-fixes.end
