#!/bin/bash
workdir=/tmp/proins-iso
scriptdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
destination=${scriptdir}/boot.iso

echo
echo "Downloading syslinux if necessary"
if [ ! -d ${scriptdir}/syslinux-6.03 ]; then
 cd ${scriptdir}
 wget https://mirrors.edge.kernel.org/pub/linux/utils/boot/syslinux/syslinux-6.03.tar.gz
 tar zfx syslinux-6.03.tar.gz
fi

echo
echo "Preparing system files"
mkdir -p ${workdir}/CD_root
cp $(realpath /boot/vmlinuz-5.4.0-45-generic) ${workdir}/CD_root/vmlinuz
cp $(realpath /boot/initrd.img-5.4.0-45-generic) ${workdir}/CD_root/initrd

echo
echo "Preparing syslinux configuration"
cp ${scriptdir}/splash.png ${workdir}/CD_root/

mkdir -p ${workdir}/CD_root/isolinux/
cp ${scriptdir}/isolinux.cfg ${workdir}/CD_root/isolinux/
cp ${scriptdir}/syslinux-6.03/bios/core/isolinux.bin ${workdir}/CD_root/isolinux/
cp ${scriptdir}/syslinux-6.03/bios/com32/elflink/ldlinux/ldlinux.c32 ${workdir}/CD_root/isolinux/
cp ${scriptdir}/syslinux-6.03/bios/com32/menu/vesamenu.c32 ${workdir}/CD_root/isolinux/
cp ${scriptdir}/syslinux-6.03/bios/com32/lib/libcom32.c32 ${workdir}/CD_root/isolinux/
cp ${scriptdir}/syslinux-6.03/bios/com32/libutil/libutil.c32 ${workdir}/CD_root/isolinux/

echo
echo "Building ISO"
mkisofs -o ${destination} -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table ${workdir}/CD_root

echo
echo "All steps completed, your ISO is ready at $(realpath ${destination})"
sleep 10

