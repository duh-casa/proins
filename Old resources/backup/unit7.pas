{+-----------------------------------------------------------------------------+}
{| RSO Progressive installer - Unit 7 / Form 7, version 0.7                    |}
{| (c) Vassugo and Dustwolf, 2020                                              |}
{|                                                                             |}
{| Graphical front-end, application window for showing simple and discrete     |}
{| progress of steps in application process. Complete method was overwritten   |}
{| from scratch, because the original "PopUp Message component" had to many    |}
{| issues to begin with. Our version is smaller, slicker and way faster        |}
{+-----------------------------------------------------------------------------+}

unit Unit7;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons;

type

  { TForm7 }

  TForm7 = class(TForm)
    BitBtn1: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Timer1: TTimer;
    Timer2: TTimer;
    procedure BitBtn1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);


  private

  public

  end;

var
  Form7: TForm7;

implementation

{$R *.lfm}

{ TForm7 }


//When info baloon is shown the original show procedure triggers timer on form 7,
//which starts to close (dimm) info baloon, when a pre-defined value of time passes.
//This value can be normally set to anything comprehendible.
procedure CloseInfoBaloon();
begin
   //For best graphics effect we first decrease AlphaBlend Value...
    Form7.Timer2.Enabled := True;
   //Though we can not see it, we still must be nice and tidy and close it, to clean up memory. :)
   Form7.Close();
end;


//The timer element is used only once, when show-baloon procedure is called.
//After triggering specified time, it deactivates itself.
procedure TForm7.Timer1Timer(Sender: TObject);
begin
   Timer1.Enabled := False;
   CloseInfoBaloon();
end;

procedure TForm7.Timer2Timer(Sender: TObject);
begin
     Form7.AlphaBlendValue := (Form7.AlphaBlendValue - 5);
     if (Form7.AlphaBlendValue = 0) then
     begin
       Form7.Timer1.Enabled := False;
       Form7.Timer2.Enabled:= False;
       Form7.AlphaBlendValue := 0;
     end;
   //Unit AlphaBlendValue hits 0. At this level, the element is practicly invisible.

end;


//The button on the balon can override predefined amount of time, in which the baloon
//is shown on the screen, thus closeing is down emediatley.
//This also has to be done correctly, so CloseInfoBaloon method is called upon.
procedure TForm7.BitBtn1Click(Sender: TObject);
begin
   Timer1.Enabled := False;
   CloseInfoBaloon();
end;


end.


{+-----------------------------------------------------------------------------+}
{|                           *** END OF FILE ***                               |}
{+-----------------------------------------------------------------------------+}


