#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
did=$(../GetDiskID/GetDiskID $2)
vars=../${did}/vars
mkdir -p ${vars}/status
efipart=$(cat ${vars}/efipart.txt)
syspart=$(cat ${vars}/syspart.txt)
source=$(cat ${vars}/source.txt)

touch ${vars}/status/03-rsync.begin

mkdir -p /mnt/${did}

mount ${syspart} /mnt/${did}
mkdir -p /mnt/${did}/boot/efi
mount ${efipart} /mnt/${did}/boot/efi

rsync -axAXH --info=progress2 erso::proins/${source}/ /mnt/${did}/
sync

ls -lah /mnt/${did} > ${vars}/status/03-rsync.test

umount /mnt/${did}/boot/efi
umount /mnt/${did}

touch ${vars}/status/03-rsync.end
