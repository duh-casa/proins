# Proins

This is a series of scripts that enable a [Ubuntu](https://ubuntu.com/) runtime environment, booted over NFS, to write a different Ubuntu-based operating system to the computer harddisk.

These scripts are intended to be used in tandem with the [RSO mirror](https://gitlab.com/duh-casa/rso-mirror) server software, which enables the aforementioned Ubuntu runtime environment.

## Usage

Please refer to the [RSO mirror](https://gitlab.com/duh-casa/rso-mirror) documentation. Proins scripts will not function without a RSO mirror server available in the local network.

The directories contain the following:
* [eject](eject) - systemd service for ejecting the CD on boot (used to remind people to remove the boot CD if used)
* [express-gui](express-gui) - binary for a GUI application for the Proins express scripts
* [iso](iso) - scripts for generating the ISO for the Proins OS bootloader CD
* [mkimage](mkimage) - scripts for generating an OS image to be used with Proins
* [scripts-express-efi](scripts-express-efi) - scripts for writing an OS image for UEFI systems
* [scripts-express](scripts-express) - scripts for writing an OS image for Legacy systems
* [scripts-usb-efi](scripts-usb-efi) - scripts for writing a Proins OS bootloader to a USB key for UEFI systems
* [scripts-usb](scripts-usb) - scripts for writing a Proins OS bootloader to a USB key for Legacy systems

There are other directories containing various legacy or unfinished scripts or other related files. 

The original purpose of the Proins project, was to quickly install an OS on a harddisk in such a way, that it securely erases the disk it is on while running and usable. With SSD drives, erasing a disk can be accomplished much faster, therefore making this special process irrelevant. The remaining express scripts currently used, are therefore OS installer scripts, built for speed.

## Installation

These scripts are designed to be run on a machine running Proins OS. Proins OS is just NFS boot Ubuntu + Proins files in `/proins`.

For instructions how to prepare the Proins OS, please refer to the [RSO mirror HQ server setup documentation](https://gitlab.com/duh-casa/rso-mirror/-/blob/master/RSOHQ.md#proins-os).

## Contributing

This software package is created for the RSO project that operates locally in Slovenia. It is made available in open source in the hopes it will be useful, for other people attempting to do something similar. As such we haven't had any need for outside contributions so far. 

If you wish to contribute please contact the existing contributors.

## License

[MIT](LICENSE)

## Project status

This code is being used in a production environment and will receive updates as practical usage requires.

