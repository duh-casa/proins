 #!/bin/bash
echo "Ta skripta bo naložila izbran disk v image"
echo
echo "V računalniku so sledeči diski:"
lsblk | grep -v loop
echo
echo "Vnesi polno ime (naprimer /dev/sda) izbranega diska iz katerega se bo naložil sistem:"
read disk

echo
echo "Vnesi številko particije (naprimer 1 za /dev/sda1), ki vsebuje EFI particijo:"
read e

echo
echo "Vnesi številko particije (naprimer 2 za /dev/sda2), ki vsebuje podatke sistema:"
read p

echo
echo "Vnesi ime image-a (npr.: chaletos):"
read instsel

if [ ${instsel} == "" ]; then
 instsel=install1
fi

did=$(../GetDiskID/GetDiskID ${disk})

if [ ${did} == "" ]; then

 echo
 echo "Napaka: Ne morem zaznati diska na ${disk}"
 exit 1

else

if [ ! -b ${disk}${e} ]; then

 echo
 echo "Napaka: Nepravilno ime EFI particije ${disk}${e}"
 exit 1

else

if [ ! -b ${disk}${p} ]; then

 echo
 echo "Napaka: Nepravilno ime sistemske particije ${disk}${p}"
 exit 1

else

old_uuid=$(lsblk ${disk}${p} -no UUID)
old_efi_uuid=$(lsblk ${disk}${e} -no UUID)

echo
echo "Nameščam disk ${did} (${disk}${p}) na proins strežnik pod imenom ${instsel} . Ali je to pravilno? [d/n]"
read confirm

if [ ${confirm} == "d" ]; then

 mkdir -p /mnt/${did}
 mount ${disk}${p} /mnt/${did}
 mkdir -p /mnt/${did}/boot/efi
 mount ${disk}${e} /mnt/${did}/boot/efi
 echo "${old_uuid}" > /mnt/${did}/home/old_uuid.txt
 echo "${old_efi_uuid}" > /mnt/${did}/home/old_efi_uuid.txt
 
 rsync -axAXH -M--fake-super --delete --exclude 'swapfil*' -e "ssh -o StrictHostKeyChecking=no" --info=progress2 /mnt/${did}/ zrcalo@erso:/data/images/${instsel}/
 rsync -axAXH -M--fake-super --delete -e "ssh -o StrictHostKeyChecking=no" --info=progress2 /mnt/${did}/boot/efi/ zrcalo@erso:/data/images/${instsel}/boot/efi/
 
 sync
 umount /mnt/${did}/boot/efi
 umount /mnt/${did}

fi

fi

fi

fi

