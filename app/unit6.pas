{+-----------------------------------------------------------------------------+}
{| RSO Progressive installer - LPR FILE version 0.7                            |}
{| (c) Vassugo and Dustwolf, 2020                                              |}
{|                                                                             |}
{| This is a simple read-only window for showing S.M.A.R.T. related log        |}
{| information file to a user. This window by itself does not include any      |}
{| programmable logic and can be / may be reused by other funcions as well.    |}
{+-----------------------------------------------------------------------------+}


unit Unit6;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons;

type

  { TForm6 }

  TForm6 = class(TForm)
    BitBtn1: TBitBtn;
    Memo1: TMemo;
    procedure BitBtn1Click(Sender: TObject);
  private

  public

  end;

var
  Form6: TForm6;

implementation

{$R *.lfm}

{ TForm6 }

procedure TForm6.BitBtn1Click(Sender: TObject);
begin
  Form6.Close();
  Form6.Caption := 'Osnovni S.M.A.R.T. izpis za pogon';
end;

end.

