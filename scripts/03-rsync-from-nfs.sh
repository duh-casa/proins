#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)
nfs=$(cat ${vars}/nfs.txt)

touch ${vars}/status/03-rsync-from-nfs.begin

mkdir -p /mnt/boot /mnt/nfs

mount ${disk}1 /mnt/boot
mount -t nfs 10.9.8.30:/nfs/${nfs} /mnt/nfs

rsync -axAX --info=progress2 /mnt/nfs/ /mnt/boot/
sync

umount /mnt/nfs
umount /mnt/boot

touch ${vars}/status/03-rsync-from-nfs.end
