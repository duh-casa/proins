#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Usage: ./00-prepare.sh authtoken /dev/sda install2 deff44d2-0dd5-4e58-87ec-4e69925ff85f"
  exit 1
fi

vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}
echo $2 > ${vars}/disk.txt
echo $3 > ${vars}/source.txt
echo $4 > ${vars}/old_uuid.txt
