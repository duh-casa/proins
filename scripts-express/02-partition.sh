#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)

touch ${vars}/status/02-partition.begin

sleep 1

parted -s -a optimal -- ${disk} mklabel msdos
parted -s -a optimal -- ${disk} mkpart primary ext4 '0%' '100%'

partprobe

sleep 1

syspart=$(lsblk ${disk} -o name -r -n | tail -n 1)
echo "/dev/${syspart}" > ${vars}/syspart.txt

syspart=$(cat ${vars}/syspart.txt)

diskMajor=$(stat -c '%t' ${disk})
diskMinor=$(stat -c '%T' ${disk})
let "diskMinor+=1"

mknod ${syspart} b ${diskMajor} ${diskMinor}

dd if=/dev/zero of=${syspart} bs=512 count=1024

mkfs.ext4 ${syspart}

partprobe

lsblk ${disk} > ${vars}/status/02-partition.test
fsck -N ${syspart} >> ${vars}/status/02-partition.test

touch ${vars}/status/02-partition.end

