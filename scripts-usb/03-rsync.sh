#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
did=$(../GetDiskID/GetDiskID $2)
vars=../${did}/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)

touch ${vars}/status/03-rsync.begin

mkdir -p /mnt/${did}

mount ${disk}1 /mnt/${did}

mkdir -p /mnt/${did}/boot/
rsync -axAXH --info=progress2 /boot/ /mnt/${did}/boot/
sync

ls -lah /mnt/${did} > ${vars}/status/03-rsync.test

umount /mnt/${did}

touch ${vars}/status/03-rsync.end
