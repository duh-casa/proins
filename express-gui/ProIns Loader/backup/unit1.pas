{+-----------------------------------------------------------------------------+}
{| RSO Progressive installer - Main Unit / Main Form, version 0.7              |}
{| (c) Vassugo and Dustwolf, 2021                                              |}
{|                                                                             |}
{| Tempoprary GUI-bootstrap, application window for setting up parameters and  |}
{| starting progressive installatioon method for RSO-to-be-ready computers     |}
{+-----------------------------------------------------------------------------+}


unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons,
  ExtCtrls, LCLType, Unix, ComCtrls, Process, Unit2;

type

  { TForm1 }

  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    ComboBox1: TComboBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Memo1: TMemo;
    Timer1: TTimer;
    Timer2: TTimer;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure ComboBox1Select(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private


//General variables to be used by this program
  public
  Selected : integer;     //Index of selected choice in installation list
  Runtime : String;       //Generated name of coresponding file with command
  Command : String;       //Actual command in string format to be used by shell
  end;

var
  Form1: TForm1;
  Selected : integer;     //Implementation of above-mentioned variables in this code
  Runtime : String;
  Command : String;



implementation

{$R *.lfm}

{ TForm1 }

//Procedure for fail-safe operation. Before each step, we confirm file presence, thus eliminating
//unknown faults and "index out of bounds" messages during runtime. Olf-fashioned, but still works. ;)
//Precursor to actual GUI-Application, whitch is still in developement, currently on-hold.


procedure UpdateList();
  begin
    // Each time, application starts, we check for file with INDEX of installations.
    If FileExists('/proins/express-gui/ProIns Loader/DATA/INSTALL_LIST')
        then
        begin
           // Presuming file has been found, we load strings into drop-down list.
           Form1.ComboBox1.Items.LoadFromFile('/proins/express-gui/ProIns Loader/DATA/INSTALL_LIST');
        end
     else
     begin
        // If something weird or bad happened, or the runtime envoiroment is faulty, we stop the appication,
        // before any colatterall damage occurs.
        Application.MessageBox('Napaka pri branju podatkov o namestitvah s ProIns strežnika, manjka datoteka INSTALL_LIST!', 'Napaka pri zagonu programa!');
        Form1.Close();
        Application.Terminate();
     end;
  end;

// Timer intervaled trigger for Updating list of installations. Timer calls for UpdateList procedure after 3/4 of a second.
// This procedure is executed only once during the application start-up.
procedure TForm1.Timer1Timer(Sender: TObject);

begin
  Timer1.Enabled := False;
  UpdateList;
end;

// Timer intervaled trigger for closing down this application. Timer calls for this method after shell installer was invoked.
// Timeout period (trigger) is set to one second.
procedure TForm1.Timer2Timer(Sender: TObject);
begin
   //Disposing the GUI of this programm, since shell installer took over
   Form1.Close();
   Application.Terminate();
end;


//Actual selection procedure, repeated each time, user changes / selects something from the drop-down list.
//Changes actual selection in drop-down list, changes corresponding descripton and runtime variables each time.
//This procedure only sets the prameters, but does not cal the actual supervisor script.
procedure TForm1.ComboBox1Select(Sender: TObject);
var
  GetCmd : TStringList;
begin

  Selected := -1;    //Index of the selected item
  Runtime := '';     //Coresponding filename with corresponding command string
  Command := '';     //Command string

  // For security purposes we check each time, that user actually selected sometning.
  Selected := ComboBox1.ItemIndex;

  If Selected = -1 then
  begin
     //If (somehow) noting is selected, we stop the actuall call and inform user.
     Application.MessageBox('Potrebno je izbrati veljavno (delujočo) namestitev', 'Napaka pri uporabniški izbiri');
  end;

  If Selected <> -1 then
  begin
     //Asuming everything went well, we set all coresponding variables with approprieate values.
     if FileExists('/proins/express-gui/ProIns Loader/DATA/' + IntToStr(Selected) + '.descritpion')
     then
        begin
           Memo1.Lines.Clear();
           Memo1.Lines.LoadFromFile('/proins/express-gui/ProIns Loader/DATA/' + IntToStr(Selected) + '.descritpion');
        end
        else
     //Next two lines are for debugging purpose only. Not to be used during production.
     //if not FileExists('/proins/express-gui/ProIns Loader/DATA/' + IntToStr(Selected) + '.descritpion')
     //then
        begin
           Memo1.Lines.Clear();
           Memo1.Lines.Add('Opis za ta namestitveni paket (še) ni na voljo. Delamo na tem... :)');
        end;

     //Setting the final command string filename
     Runtime := ('/proins/express-gui/ProIns Loader/DATA/' + IntToStr(Selected) + '.runtime');
     // Showing string for debug only
     // ShowMessage(Runtime);

       // Setting the actual command string, for shell interpretation
       GetCmd := TStringList.Create();
       GetCmd.LoadFromFile('/proins/express-gui/ProIns Loader/DATA/' + IntToStr(Selected) + '.runtime');
       Command := GetCmd.Strings[0];
       // Showing string for debug only
       // ShowMessage(Command);
       GetCmd.Free();
  end;
end;


//Procedure for verification of user-delegated cancel command
procedure TForm1.BitBtn2Click(Sender: TObject);
var
  Reply : Integer;
  BoxStyle : Integer;
begin
  // Using dialog boxes we request for user confirmation, before we terminate the application.
  BoxStyle := MB_ICONQUESTION + MB_YESNO;
  Reply := Application.MessageBox('Ali ste prepričani, da želite prekiniti?' + sLineBreak + 'Namestitev še ni dokončana!', 'Potrditev prekinitve postopka', BoxStyle);
  if Reply = IDYES then
     begin
        Application.MessageBox('Prekinitev potrjena. Program se bo sedaj zaprl.', 'Prekinitev postopka potrjena',MB_ICONINFORMATION);
        Form1.Close();
        Application.Terminate();
     end
    else
    begin
       //Do nothing
    end;
end;

procedure TForm1.BitBtn3Click(Sender: TObject);
begin
   Form2.ShowModal();
end;



procedure TForm1.BitBtn1Click(Sender: TObject);
var
  Reply : Integer;
  BoxStyle : Integer;
  Operation : TProcess;
begin
  // Using dialog boxes we request for user confirmation, before we actually call the installer script.
  BoxStyle := MB_ICONQUESTION + MB_YESNO;
  Reply := Application.MessageBox('Ali ste prepričani, da želite nadaljevati?' + sLineBreak + 'Po tej potrditvi se bo dejansko zagnal namestilnik za izbrano verzijo sistema.', 'Potrditev izbrane namestitve', BoxStyle);
  if Reply = IDYES then
     begin
        //Actually calling corresponding command string this time.
        //You have to be carefull here, everything is parsed to /bin/bash without security check!
         Operation := TProcess.Create(nil);
         Operation.ShowWindow:=swoShowNormal;
         Operation.Options := Operation.Options + [poNewConsole];
         Operation.Executable := Command;
         //The command below was deprecated and replaced with command above this comment.
         //Operation.CommandLine := (Command);
         Operation.Execute();
         Application.ProcessMessages();
         Operation.Free();
         ShowMessage('Dela!');
         //When shell installer is invoked, there is no need for this application to continiue hogging
         //resoruces, so timed procedue stops it and invokes memory recollection.
         //Procedure is invoked exactly one second after shell installer is triggered.
         Timer2.Enabled := True;
    end
    else
    begin
       //Informing user, the procedure was actually cancelled.
       Application.MessageBox('Prekinitev izbire potrjena. Prosimo, ponovno izberite veljavno namestitev.', 'Prekinitev izbire',MB_ICONINFORMATION);
    end;
end;
end.




{+-----------------------------------------------------------------------------+}
{|                           *** END OF FILE ***                               |}
{+-----------------------------------------------------------------------------+}


