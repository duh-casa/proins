#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)
old_uuid=$(cat ${vars}/old_uuid.txt)
new_uuid=$(lsblk -no UUID ${disk}1)

touch ${vars}/status/04-grub-install.begin

mkdir -p /mnt/boot
mount ${disk}1 /mnt/boot

/usr/sbin/grub-install --recheck --no-floppy --boot-directory=/mnt/boot/boot ${disk}
sed -i "s/${old_uuid}/${new_uuid}/g" /mnt/boot/boot/grub/grub.cfg
sed -i "s/${old_uuid}/${new_uuid}/g" /mnt/boot/etc/fstab
sync

umount /mnt/boot

touch ${vars}/status/04-grub-install.end
