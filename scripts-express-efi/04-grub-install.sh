#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
did=$(../GetDiskID/GetDiskID $2)
vars=../${did}/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)
efipart=$(cat ${vars}/efipart.txt)
syspart=$(cat ${vars}/syspart.txt)
old_uuid=$(cat ${vars}/old_uuid.txt)
new_uuid=$(lsblk -no UUID ${syspart})
new_efi_uuid=$(lsblk -no UUID ${efipart})

touch ${vars}/status/04-grub-install.begin

mkdir -p /mnt/${did}
mount ${syspart} /mnt/${did}
mount ${efipart} /mnt/${did}/boot/efi

if [ -f /mnt/${did}/home/old_uuid.txt ]; then
 old_uuid=$(cat /mnt/${did}/home/old_uuid.txt)
fi

sed -i "s/${old_uuid}/${new_uuid}/g" /mnt/${did}/boot/grub/grub.cfg
sed -i "s/${old_uuid}/${new_uuid}/g" /mnt/${did}/boot/efi/EFI/ubuntu/grub.cfg
sed -i "s/${old_uuid}/${new_uuid}/g" /mnt/${did}/etc/fstab

sed -e "/boot\/efi/ s/^#*/#/" -i /mnt/${did}/etc/fstab

mount -t proc proc /mnt/${did}/proc
mount -t sysfs sys /mnt/${did}/sys
mount -o bind /dev /mnt/${did}/dev
mount -t devpts pts /mnt/${did}/dev/pts

mount -t efivarfs none /mnt/${did}/sys/firmware/efi/efivars

chroot /mnt/${did} grub-install

umount /mnt/${did}/sys/firmware/efi/efivars
umount /mnt/${did}/dev/pts
umount /mnt/${did}/dev
umount /mnt/${did}/sys
umount /mnt/${did}/proc

sync

dd bs=512 count=1 if=${disk} 2>/dev/null | strings > ${vars}/status/04-grub-install.test
cat /mnt/${did}/etc/fstab >> ${vars}/status/04-grub-install.test
ls -lah /mnt/${did}/boot/efi >> ${vars}/status/04-grub-install.test

umount /mnt/${did}/boot/efi
umount /mnt/${did}

touch ${vars}/status/04-grub-install.end
