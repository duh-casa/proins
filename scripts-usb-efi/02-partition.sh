#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)

touch ${vars}/status/02-partition.begin

parted -s -a optimal -- ${disk} \
	mklabel msdos \
	mkpart primary fat32 '0%' '512MB' \
	set 1 esp on \
	mkpart primary ext4 '512MB' '100%'

partprobe

diskMajor=$(stat -c '%t' ${disk})
diskMinor=$(stat -c '%T' ${disk})
let "diskMinor+=1"
mknod ${disk}1 b ${diskMajor} ${diskMinor}

let "diskMinor+=1"
mknod ${disk}2 b ${diskMajor} ${diskMinor}

dd if=/dev/zero of=${disk}1 bs=512 count=1024
dd if=/dev/zero of=${disk}2 bs=512 count=1024

mkfs.fat -F32 ${disk}1
mkfs.ext4 ${disk}2

partprobe

lsblk ${disk} > ${vars}/status/02-partition.test
fsck -N ${disk}1 >> ${vars}/status/02-partition.test
fsck -N ${disk}2 >> ${vars}/status/02-partition.test

touch ${vars}/status/02-partition.end

