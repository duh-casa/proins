{+-----------------------------------------------------------------------------+}
{| RSO Progressive installer - LPR FILE version 0.7                            |}
{| (c) Vassugo and Dustwolf, 2020                                              |}
{|                                                                             |}
{| This is primary LPR object build file for this application.                 |}
{| See other unit files for details upon each seperate class.                  |}
{| This file also holds definitions for splash file, when installer starts.    |}
{+-----------------------------------------------------------------------------+}

program progressive_installer_app;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}

  cthreads,
  {$ENDIF}{$ENDIF}

  Interfaces, // this includes the LCL widgetset
  Forms, Unit1, Unit2, Unit3, Unit4, Unit5, Unit7, splScreen, Unit6,
  pkg_gifanim;


{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Title:='RSO OEM Installer';
  Application.Scaled:=True;
  Application.Initialize;
  //Splash screen is called upon pre-fedined form inside application package
  Splash:=Tsplash.create(nil);
  Splash.Show;
  Splash.Update;
  //While all the necesary modules are being loaded, splash screen is being shown
  Application.ProcessMessages;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(TForm6, Form6);
  Application.CreateForm(TForm7, Form7);
  //Application.CreateForm(TSplash, Splash);
  //When loading is complete and aplication can start, SplashScreen is closed
  //and disposed of.
  Splash.Close;

  Application.Run;
end.


{+-----------------------------------------------------------------------------+}
{|                           *** END OF FILE ***                               |}
{+-----------------------------------------------------------------------------+}

