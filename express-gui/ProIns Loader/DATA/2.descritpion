KDE Neon - pripravljen za mlajše

Operacijski sistem je pripravljen za mlajše prejemnike. Izdelan z različico "User edition", prenešeno 17.2.2023 z uradne spletne strani KDE Neon. Primeren je za večjedrne računalnike, izdelane po l. 2012. Navodila na namizju bralca tikajo.

Zadnja posodobitev namestitvenega odtisa: 23.6.2024

---------------------------------

Zgodovina sprememb:

23.6.2024:
    - posodobitev sistema in aplikacij

16.6.2024:
    - urejene datotečne asocijacije z nameščenim paketom LibreOffice
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

2.6.2024:
    - posodobitev sistema in aplikacij

26.5.2024:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

19.5.2024:
    - poslovenjen Thunderbird
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

12.5.2024:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

28.4.2024:
    - nameščen modul brightnessctl
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

21.4.2024:
    - v GRUB-u vključen OS_prober
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

13.4.2024:
    - posodobljena navodila Preberi nujno !!!, ki so na namizju
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

7.4.2024:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

3.4.2024:
    - izklopljeni orodni namigi v Dolphinu
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

24.3.2024:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

17.3.2024:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

10.3.2024:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

4.3.2024:
    - dodanih nekaj novih povezav (Chat GPT, Plasma 6 itd.)
    - odstranjene tuje pisave, dodanih kup novih
    - v pultu zdaj ura prikazuje tudi sekunde
    - posodobitev sistema in aplikacij

3.3.2024:
    - nadgradnja sistema na Plasma 6
    - Vremenska napoved odtranjena z namizja in omogočena v pultu pri uri
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

24.2.2024:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

18.2.2024:
    - GRUB spremenjen na poslovenjeno temo KDE Neon
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

11.2.2024:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

4.2.2024:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

29.1.2024:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

23.1.2024:
    - urejeno generiranje novega naslova v AnyDesk
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

20.1.2024:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

14.1.2024:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

7.1.2024:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

3.1.2024:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

25.12.2023:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

10.12.2023:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

3.12.2023:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

26.11.2023:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

19.11.2023:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

12.11.2023:
    - odstranjen paket LibreOffice (snap)
    - nameščen paket LibreOffice (.deb)
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

5.11.2023:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

28.10.2023:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

18.10.2023:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

6.10.2023:
    - Teamviewer odstranjen zaradi spremenjenih pogojev uporabe
    - za oddaljeno pomoč nameščen program AnyDesk
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

1.10.2023:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)
    - odstranjen /etc/apt/sources.list.d/proxsign.list (zaradi obvestila o potečenem ključu ob posodobitvah)

17.9.2023:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)
    - posodobljena navodila PREBERI NUJNO!!! - dodan zaznamek, ki vodi v forum na-prostem.si

10.9.2023:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

13.8.2023:
    - posodobitev sistema in aplikacij

8.8.2023:
    - izgled in občutek prirejen iz različice Neon za starejše (težava pri izvornem disku):
        - Plasma theme: Deepin20 Light
        - posodobljena navodila na namizju
        - statično ozadje Terra Nova
        - komentarji programov na namizju popravljeni (vikanje -> tikanje)
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)
    - posodobljeni .txt datoteki old_uuid in old_efi_uuid

24.7.2023:
    - posodobitev sistema in aplikacij
    - brisanje starih kernelov (autoclean, autoremove --purge)

10.7.2023:
    - "preskok" na usb različico Neona, ki je bila pripravljena istočasno ves čas vzporedno vzdrževana za namen projekta Na Prostem
    - posodobitev sistema in aplikacij
    - prenos skript ipd. v mapo /home
    - namestitev začelja WINE
    - namestitev Gcompris
    - namestitev aplikacije Diski
    - brisanje starih kernelov (autoclean, autoremove --purge)


29.6.2023:
    - dodani gonilniki za Broadcom wifi kartice
    - posodobitev sistema in programov
    - brisanje starih kernelov (autoclean, autoremove --purge)

19.6.2023:
    - posodobitev sistema in programov
    - brisanje starih kernelov (autoclean, autoremove --purge)

13.6.2023:
    - posodobitev sistema in programov
    - brisanje starih kernelov (autoclean, autoremove --purge)

31.5.2023:
    - posodobitev sistema in programov
    - brisanje starih kernelov (autoclean, autoremove --purge)
    - TeamViewer dodan med priljubljene aplikacije

27.5.2023:
    - posodobitev sistema in programov
    - brisanje starih kernelov (autoclean, autoremove --purge)
    - nameščen TeamViewer
    - vklopljen auto-NumLock (Nastavitve/Tipkovnica)
    - urejeni manjkajoči prevodi v Zaganjalniku programov


16.4.2023:
    - posodobitev sistema in programov
    - brisanje starih kernelov (autoclean, autoremove --purge)


31.3.2023:
    - posodobitev sistema in programov

7.3.2023:
    - posodobitev sistema in programov
    - urejen swapfile
    - GRUB urejen in se prikazuje (4 sekunde)


18.2.2023:
    - izvorna namestitev sistema in programov
    - dodanih nekaj tem in elementov izgleda
    - CGP in Plymouth urejena na videz RSO
    - urejene povezave v brskalnikih
    - poslovenjeni in urejeni vnosi v Zaganjalniku programov
    - skripte in .txt prenešeni v /home mapo
    - posodobitev sistema + autoclean --purge





Zadnja posodobitev namestitvene slike: 7. 4. 2024