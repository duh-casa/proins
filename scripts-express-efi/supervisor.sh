#!/bin/bash
echo "Ta skripta bo namestila izbran sistem na lokalni disk"
echo

cd /proins/scripts-express-efi

echo "V računalniku so sledeči diski:"
lsblk | grep -v loop
echo
echo "Vnesi polno ime (naprimer /dev/sda) izbranega diska ki se bo izbrisal in na katerega se bo namestil sistem:"
read disk

did=$(../GetDiskID/GetDiskID ${disk})

if [ ${did} == "" ]; then

 echo
 kdialog --title "Napaka!" --error "Nepravilen vnos, oziroma ne morem zaznati diska na ${disk} \n \n"
 exit 1

else

echo
echo
echo "Izbral si disk ${disk} (${did}). Ali je to pravilno? [d/n]"
read confirm

if [ ${confirm} == "d" ]; then

instsel=$1
# install2
uuid=$2
# deff44d2-0dd5-4e58-87ec-4e69925ff85f

echo "Pripravljam za namestitev"
./00-prepare.sh authtoken ${disk} ${instsel} ${uuid}

vars=../$(../GetDiskID/GetDiskID ${disk})/vars
mkdir -p ${vars}/status

if [ ! -f ${vars}/status/01-wipe-partitions.end ]; then
 echo
 echo "Brišem obstoječe particije"
 ./01-wipe-partitions.sh authtoken ${disk}
fi

if [ -f ${vars}/status/01-wipe-partitions.end ]; then
 if [ ! -f ${vars}/status/02-partition.end ]; then
  echo
  echo "Ustvarjam nove particije"
  ./02-partition.sh authtoken ${disk}
 fi
fi

if [ -f ${vars}/status/02-partition.end ]; then
 if [ ! -f ${vars}/status/03-rsync.end ]; then
  echo 
  echo "Kopiram sistemske datoteke"
  ./03-rsync.sh authtoken ${disk}
 fi
fi

if [ -f ${vars}/status/03-rsync.end ]; then
 if [ ! -f ${vars}/status/04-grub-install.end ]; then
  echo
  echo "Nameščam zaganjalnik GRUB"
  ./04-grub-install.sh authtoken ${disk}
 fi
fi

if [ -f ${vars}/status/04-grub-install.end ]; then
 if [ ! -f ${vars}/status/05-swap.end ]; then
  echo
  echo "Dodajam swap in montiram EFI"
  ./05-swap-n-efi.sh authtoken ${disk}
 fi
fi

if [ -f ${vars}/status/05-swap.end ]; then
 if [ ! -f ${vars}/status/06-fixes.end ]; then
  echo
  echo "Izvajam popravke"
  ./06-fixes.sh authtoken ${disk}
 fi
fi


echo
echo "Generiram poročilo"
./99-upload.sh authtoken ${disk}

echo
echo "Vsi nedokončani koraki zaključeni"
kdialog --title "Uspešno zaključena namestitev!" --msgbox "Namestitev ${instsel} uspešno zaključena na $(../GetDiskID/GetDiskID ${disk}).\n Sedaj lahko ponovno zaženete računalnik.\n \n"

else
 kdialog --title "Napaka!" --error "Uporabnik prekinil namestitev \n \n"
 exit 1
fi

fi

