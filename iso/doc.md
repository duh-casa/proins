# ISOLINUX for Proins

## Steps

1. Download
https://mirrors.edge.kernel.org/pub/linux/utils/boot/syslinux/syslinux-6.03.tar.gz
tar zfx syslinux-6.03.tar.gz

2. mkdir CD_root

3. cp splash.png CD_root

4. cp $(realpath /boot/vmlinuz) CD_root/vmlinuz

5. cp $(realpath /boot/initrd.img) CD_root/initrd

6. mkdir CD_root/isolinux

7. cp isolinux.cfg CD_root/isolinux/

8. cp syslinux-6.03/bios/core/isolinux.bin CD_root/isolinux/

9. cp syslinux-6.03/bios/com32/elflink/ldlinux/ldlinux.c32 CD_root/isolinux/

10. cp syslinux-6.03/bios/com32/menu/vesamenu.c32 CD_root/isolinux/

11. cp syslinux-6.03/bios/com32/lib/libcom32.c32 CD_root/isolinux/

12. cp syslinux-6.03/bios/com32/libutil/libutil.c32 CD_root/isolinux/

13. mkisofs -o boot.iso -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table CD_root



