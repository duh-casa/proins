#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)

touch ${vars}/status/05-prepare-cow.begin

mkdir -p /mnt/install
mount ${disk}2 /mnt/install

mkdir -p /mnt/install/rw
mkdir -p /mnt/install/work
sync

umount /mnt/install

touch ${vars}/status/05-prepare-cow.end
