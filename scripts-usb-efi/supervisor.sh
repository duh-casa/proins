#!/bin/bash
echo "Ta skripta bo namestila proins zaganjalnik na izbrani medij"
echo

cd /proins/scripts-usb-efi

echo "V računalniku so sledeči mediji:"
lsblk | grep -v loop
echo
echo "Vnesi polno ime (naprimer /dev/sda) izbranega medija ki se bo izbrisal in na katerega se bo namestil proins zaganjalnik:"
read disk

echo
echo
echo "Izbral si medij ${disk} (${did}). Ali je to pravilno? [d/n]"
read confirm

if [ ${confirm} == "d" ]; then

echo "Pripravljam za namestitev"
./00-prepare.sh authtoken ${disk}

vars=../$(../GetDiskID/GetDiskID ${disk})/vars
mkdir -p ${vars}/status

if [ ! -f ${vars}/status/01-wipe-partitions.end ]; then
 echo
 echo "Brišem obstoječe particije"
 ./01-wipe-partitions.sh authtoken ${disk}
fi

if [ -f ${vars}/status/01-wipe-partitions.end ]; then
 if [ ! -f ${vars}/status/02-partition.end ]; then
  echo
  echo "Ustvarjam nove particije"
  ./02-partition.sh authtoken ${disk}
 fi
fi

if [ -f ${vars}/status/02-partition.end ]; then
 if [ ! -f ${vars}/status/03-rsync.end ]; then
  echo 
  echo "Kopiram sistemske datoteke"
  ./03-rsync.sh authtoken ${disk}
 fi
fi

if [ -f ${vars}/status/03-rsync.end ]; then
 if [ ! -f ${vars}/status/04-grub-install.end ]; then
  echo
  echo "Nameščam zaganjalnik GRUB"
  ./04-grub-install.sh authtoken ${disk}
 fi
fi


echo
echo "Generiram poročilo"
./99-upload.sh authtoken ${disk}

echo
echo "Vsi nedokončani koraki zaključeni"
kdialog --title "Uspešno zaključena namestitev!" --msgbox "Namestitev proins zaganjalnika uspešno zaključena na $(../GetDiskID/GetDiskID ${disk}).\n Sedaj lahko ponovno zaženete računalnik.\n \n"

else
 kdialog --title "Napaka!" --error "Uporabnik prekinil namestitev \n \n"
 sleep 10
fi

