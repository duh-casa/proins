#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
gvars=$(../GetDiskID/GetDiskID $2)/vars
vars=../$gvars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)

touch ${vars}/status/04-rsync-proins.begin

mkdir -p /mnt/boot
mount ${disk}1 /mnt/boot

mkdir -p /mnt/boot/proins
rsync -axAX /proins/ /mnt/boot/proins/
sync

mkdir -p /mnt/boot/proins/${gvars}/status
touch /mnt/boot/proins/${gvars}/status/04-rsync-proins.end
sync
umount /mnt/boot

touch ${vars}/status/04-rsync-proins.end
