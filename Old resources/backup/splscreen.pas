{+-----------------------------------------------------------------------------+}
{| RSO Progressive installer - Main Unit / Main Form, version 0.7              |}
{| (c) Vassugo and Dustwolf, 2020                                              |}
{|                                                                             |}
{| Graphical splash screen used only for motivation fo users, while the main   |}
{| program is loading into memory and while all the critical funcions of this  |}
{| installer application are gathering and storin information into memory.     |}
{+-----------------------------------------------------------------------------+}


unit splScreen;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  GifAnim;

type

  { TSplash }

  TSplash = class(TForm)
    GifAnim1: TGifAnim;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Timer1: TTimer;
    Timer2: TTimer;
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private

  public

  end;

var
  Splash: TSplash;

implementation

{$R *.lfm}

{ TSplash }

procedure TSplash.Timer1Timer(Sender: TObject);
begin
  Label2.Caption := 'Samo še čisto malo...';
  Splash.Update();
  Timer1.Enabled := False;
  Timer2.Enabled := True;
end;

procedure TSplash.Timer2Timer(Sender: TObject);
begin
   Label2.Caption := 'Preverjamo še zadnje malenkosti...';
   Splash.Update();
   Timer2.Enabled := False;
end;







end.

