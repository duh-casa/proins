//+-----------------------------------------------------------------------------
//| A simple little Object Pascal program, used in project ProIns as a "time"  |
//| tempered training programm. Used in dummy-scripts, it represents actual    |
//| processing of each command in each script and can be set for different     |
//| time scopes, representing different payloads under executing various       |
//| commands in the process of so called progressive installation.             |
//| The program accepts only one parameter in form of "time" (expressed in ms) |
//| and will literally "eat" time for specified duration.                      |
//| TimeEater is being used by Vassugo and DustWolf in project ProIns, however,|
//| if you find a usefull note over it, go ahead, let there be light. :)       |
//+----------------------------------------------------------------------------+

program TimeEater(Duration, Stage);

{$mode objfpc}{$H+}    //Thou shalt not change this line. Ever.

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}    //Not using extra threads, but must be
  cthreads,                            //loaded for proper syntax-handling.
  {$ENDIF}{$ENDIF}
  Classes, SysUtils, Process, Unix;
  //We only need one thread for this job and no special classes to load.

var
  TimeDuration : Integer;   //Global variable, used for "measuring time duration".
  ArgumentIsNumber : Boolean;
  TimeDurationString : String;
  AChar : Char;
  Stage : String;

procedure GiveMeHelp();  //Simple procedure that writes out the help section of a program.
begin
   WriteLn('');
   WriteLn('A simple little program used with "training-wheels" scripts for');
   WriteLn('simulating various payloads and time needed for execution of ');
   WriteLn('consequent commands in each script. Basicly it waits n-milliseconds,');
   WriteLn('specified quantity of time, expressed as a calling-parameter.');
   WriteLn('Output is a plain text file with .TETest suffix, as a result');
   WriteLn('and can be used / piped into other Unix-like programs and scripts.');
   WriteLn('');
   WriteLn('Usage:  TimeEater [DURATION] [SCRIPT_NUMBER]   |   Example: TimeEater 2500 5');
   WriteLn('        TimeEater [ARGUMENT]                   |   Example: TimeEater -h');
   WriteLn('');
   WriteLn('The -h or --help parameter has obviosly brought you here so no additional');
   WriteLn('explaining is needed for that. Since the program is really short and stupid, ');
   WriteLn('so should be the help section.');
   WriteLn('');
   WriteLn('Exit status codes:');
   WriteLn('0: Everything is fine, program completed correctly.');
   WriteLn('1: Missing or inadequate arguments');
   WriteLn('2: Access violation, cannot generate output file. Write permissions?');
   WriteLn('3: Unsuspected, user related, but probably serious (user) problems.');
   WriteLn('');
   WriteLn('🄯 Author: Vassugo for ProIns projetct, 2020');
end;


procedure TimeWaster();  //Actual procedure for "waisting time", called only if certain parameter is engaged.
var
  Step : Integer;
  ContentString : String;
begin
  Step := 0;
  //Using loop and Sleep function, we "eat" time for specified amount of time.
  While (Step < TimeDuration) do
  begin
     Sleep(1);
     Step := Step + 1;
  end;
  Sleep(1); //We wait for additional one millisecond, to properly achieve the requested time duration.

  //Setting the string for result output, to be written into file.
  ContentString := IntToStr(TimeDuration);

  //If second, debugging parameter was introduced, we add it to final output to the file.
  if not (Stage = '') then
  begin
     ContentString := ContentString + '.......' + Stage;
  end;

  fpsystem('echo ' + DateTimeToStr(Now) + '.......' + ContentString + ' >> TestResult.TWTest');
  //Jump-back point into main program.
end;



//The execution block of TimeEater program is listed here.

begin
  TimeDurationString := (paramStr(1));  //Set string format of first argument, we need it for checking numeric properties.
  ArgumentIsNumber := False;  //Pre-set boolean, asuming user might do a (non)intentional typo.

  //Second parameter is actually a debug and "hidden" parameter, asignating the number of the script for testing purposes.
  //It is not actually necesary for succesfull completion of this program, but very helpfull when running with "training-wheels" scripts.
  If not ((paramStr(2) = '')) then
  begin
     Stage := (paramStr(2));
  end;

  //Checking, if first-call argument is actually numeric.
  for AChar in TimeDurationString do
  begin
    ArgumentIsNumber := AChar in ['0'..'9'];
    if not ArgumentIsNumber then
    begin
       ArgumentIsNumber := False;
       Break;
    end;
  end;

  if (paramStr(1) = '') then   //If there is no parameters, the exception is raised.
  begin
    WriteLn('Not enough arguments. Missing time argument (e.g. 2500 ).');
    WriteLn('');
    Halt(1);  //Exception 1 is logged.
  end;
  if (paramStr(1) = '-h') then   //Calling for help.
  begin
    GiveMeHelp();
    Halt(0); //Normal stop, so no exception is logged.
  end;
  if (paramStr(1) = '--h') then    //Calling for help.
  begin
    GiveMeHelp();
    Halt(0); //Normal stop, so no exception is logged.
  end;
  if (paramStr(1) = '-help') then  //Calling for help.
  begin
    GiveMeHelp();
    Halt(0); //Normal stop, so no exception is logged.
  end;
  if (paramStr(1) = '--help') then  //Calling for help.  There are four if's for same thing here, i noticed that too,
  begin                             //and it's not idiot programming, but Object Pascal compiler limitations. :)
    GiveMeHelp();
    Halt(0); //Normal stop, so no exception is logged.
  end;


  if (ArgumentIsNumber) then  //If called argument is a proper numeric value, we grant the execution block.
  begin
     if (StrToInt((paramStr(1))) > 0) then     //If the number is greater than 0, we cast it to specified variable and run the procedure.
     begin
       TimeDuration := StrToInt((paramStr(1)));
       WriteLn('Wasting time for ' + TimeDurationString + ' milliseconds...');  //Short notice on screen for the user, just in case.
       TimeWaster();
       Halt(0);  //Everything went out fine and dandy, so we finally end the program.
       //Program.stop;
     end
     else   //Now this one is simply a easter-egg. Just a angry typo error messeage.  :)
        WriteLn('Well, what did you expect? A bunny rabbit!?');
        WriteLn('Simon says: Garbage in, garbage out!');
        WriteLn();
        WriteLn();
        Halt(4);  //And exception is logged anyway, but who cares.
  end;
  Halt(3);  //If nothing of above is adequate, something must went horribly wrong and we log exception for debugging.
end.


//+----------------------------END OF FILE-------------------------------------+
