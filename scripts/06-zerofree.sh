#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)

touch ${vars}/status/06-zerofree.begin

zerofree -v ${disk}1

touch ${vars}/status/06-zerofree.end
