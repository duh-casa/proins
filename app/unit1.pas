{+-----------------------------------------------------------------------------+}
{| RSO Progressive installer - Main Unit / Main Form, version 0.7              |}
{| (c) Vassugo and Dustwolf, 2020                                              |}
{|                                                                             |}
{| Graphical front-end, application window for setting up parameters and       |}
{| starting progressive installatioon method for RSO-to-be-ready computers     |}
{+-----------------------------------------------------------------------------+}


unit Unit1;

{$mode objfpc}{$H+}

interface

// Important: Overload calls must be implemented in each unit - for each unit :)
uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, EditBtn,
  Buttons, Menus, ExtCtrls, ComCtrls, Process, Unit2, Unit3, Unit4, Unit5, Unit6,
  Unit7, GifAnim, LCLType, Unix;

type

  { TForm1 }

  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    Label1: TLabel;
    Label2: TLabel;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    N1: TMenuItem;
    ProgressBar1: TProgressBar;
    ProgressBar2: TProgressBar;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure MenuItem8Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;


implementation

{$R *.lfm}

{ TForm1 }

//Globally asigned variables for handling bash-command-line output.
//The "UserDiskSelected" is used in more than one procedure and must
//be declared globally.
var
   s : ansistring;
   UserDiskSelected : string;
   LastDateTime : string;
   WorkingStatus : Integer;
   RunDir : String;



//Logging procedure used throughout the application, writing timestamps and
//application relevant run-time data to single, pre-defined plain text file.
procedure AddToLog(LogEntry : string);
var
   TimeStamp : string;
begin
   TimeStamp := FormatDateTime('DD. MM. YYYY - hh:mm:ss', now);
   fpsystem('echo ' + TimeStamp + ' - ' + LogEntry + ' >> ' + RunDir + '/proins_runlog.log');
end;


//This is just a future set-up-point for script flow and control handling.
//Some of the big things are about to happen here.
procedure GetRunningState();
begin
   //Calling this procedure just for testing

end;



//Very important procedure while starting and stopping this installer as a software
//package is a kind of self-awarness. This is achieved trough memorising serial number
//of a hard driver and storing all relevant information in a directory, named by
//that serial number. The Subdirectory mentioned will of course be a special NFS
//mount on a dedicated server, so path to specific sub-directories will be hard-wired there.
procedure SetWorkingDirectory();
var
   Operation : TProcess;
   ListLines : TStringList;
   TargetDisk : String;
   DiskFolder : String;
begin
   RunDir := ('/proins/app');   //Pre-init of needed variables, needed for proper function of logging service.
   TargetDisk := ('');   //We set empty string on this variable, so we can stop program if there are no attached devices.
   AddToLog('[APP-INFO]-Inicializacija in zagon aplikacije in dnevniške storitve.');
   AddToLog('[APP-EXEC]-Aplikacija je začela s preverjanjem absolutne poti do prvega fizičnega diska.');
   //Checking for attached "/dev/sda" or "/dev/sdb/" in two subsequent if cases. Statisticly, the first physical drive should
   //be either of those two. If actual physical drive is */dev/sdc or "higher" alpahbetically, there will be hell of a problem here.
   //Note to myself: FIGURE THIS ONE OUT PROPERLY!
   Operation := TProcess.Create(nil);
   ListLines := TStringList.Create;
   Operation.Options := Operation.Options + [poWaitOnExit, poUsePipes];
   Operation.CommandLine := ('lsblk /dev/sda');
   Operation.Execute();
   Application.ProcessMessages();
   ListLines.LoadFromStream(Operation.Output);
   if (ListLines.Count > 2) then
   begin
      TargetDisk := ('/dev/sda');
      Operation.Free();
      ListLines.Free();
      AddToLog('[APP-INFO]-Aplikacija je uspešno potrdila, da je prvi fizično priključeni disk v sistemu /dev/sda.');
   end
   else
   begin
      Operation.Free();
      ListLines.Free();
      Operation := TProcess.Create(nil);
      ListLines := TStringList.Create;
      Operation.Options := Operation.Options + [poWaitOnExit, poUsePipes];
      Operation.CommandLine := ('lsblk /dev/sdb');
      Operation.Execute();
      Application.ProcessMessages();
      ListLines.LoadFromStream(Operation.Output);
      if (ListLines.Count > 2) then
      begin
         TargetDisk := ('/dev/sdb');
         Operation.Free();
         ListLines.Free();
         AddToLog('[APP-INFO]-Aplikacija je uspešno potrdila, da je prvi fizično priključeni disk v sistemu /dev/sdb.');
      end;
   end;

   //If there are no hard drives attached, exception is raised, user is warned and application stopps.
   //Application needs at least one properly attached block device afrer all.
   If (TargetDisk = '') then
   begin
      Application.MessageBox('V tem računalniku ne morem prepoznati nobene blokovne naprave!' + #13#10 + 'Namestilnik potrebuje vsaj en fizično priključen disk v sistemu!' + #13#10 + #13#10 + 'Preverite, da je vsaj en disk pravilno priključen in ponovno zaženite sistem ter RSO namestilnik!', 'Kritična napaka, zaustavljam program', MB_ICONHAND);
      Application.Terminate;
   end;

   //Assuming, we confirmed the first attached disk, now we have to configure the WorkingDirectory variable.
   Application.ProcessMessages();
   Operation := TProcess.Create(nil);
   ListLines := TStringList.Create;
   Operation.Options := Operation.Options + [poWaitOnExit, poUsePipes];
   Operation.CommandLine := ('/proins/binaries/GetDiskID ' + TargetDisk);
   Operation.Execute();
   Application.ProcessMessages();
   ListLines.LoadFromStream(Operation.Output);
   AddToLog('[APP-OK]-Aplikacija je uspešno nastavila pod do delovnega direktorija.');
   AddToLog('[APP-EXEC]-Preverjam, ali direktorij že obstaja in ali je namestilnik že bil zagnan.');
   DiskFolder := ListLines.Strings[0];
   RunDir := ('/proins/vars/' + DiskFolder);
   Application.ProcessMessages();
   ListLines.Free();
   Operation.Free();

   //Finally, we have to check, if the "disk model number and serial number" directory exists.
   if DirectoryExists(RunDir) then
   begin
      //In this case, we have allready set baseline for installation process, so we set the status variable and continue.
      WorkingStatus := 1;
      AddToLog('[APP-INFO]-Direktorij za ta sistem že obstaja, nadaljujem z instalacijo.');
      AddToLog('[APP-EXEC]-Generiram datoteko s časovnim žigom trenutnega (zadnjega) zagona zaganjalnika...');
      fpsystem('echo ' + (FormatDateTime('DD. MM. YYYY - hh:mm:ss', now)) + ' > ' + RunDir + '/vars/installer_last_start.log');
      AddToLog('[APP-OK]-Datoteka s časovnim žigom trenutnega (zadnjega) zagona zaganjalnika uspešno generirana.');
   end
   else
   begin
      //If directory is not in place, we have to create it and inform status handler, that we are just about to begin all the procedures.

      //In first step, we have to create "root" directory for this session (or system, i.e. diskmodel_diskserialNumber).
      AddToLog('[APP-WARN]-Izgleda, da je to prvi zagon namestilnika, podmapa tega sistema ne obstaja..');
      AddToLog('[APP-EXEC]-Generiram novo podmapo z absolutno potjo: ' + RunDir + '.');
      fpsystem('mkdir -p ' + RunDir);
      AddToLog('[APP-OK]-Uspešna inicializacija aplikacije in dnevniške storitve.');
      AddToLog('[APP-OK]-Podmapa sistema ' + RunDir + ' uspešno generirana.');
      fpsystem('mkdir -p ' + RunDir + '/vars');
      //Following block of code is intended to preprare directory and file structure for newly created
      //"root" directory of specific system in question. For security reasons, we are doing it more carefully,
      //than it should probably have to be. But than again, better to be safe than sorry.
      //Marking all the steps in the log, as we proceed. In first step, we have to move the log file itself.
      AddToLog('[APP-INFO]-Kopiram datoteke namestilnika in skripte v novo podmapo...');
      AddToLog('[APP-EXEC]-Poizkušam premakniti dnevniško datoteko...');
      fpsystem('cp /proins/proins/proins_runlog.log ' + RunDir + 'proins_runlog.log');
      fpsystem('rm -rf /proins/app/proins_runlog.log');
      AddToLog('[APP-OK]-Dnevniška datoteka uspešno premaknjena.');
      AddToLog('[APP-WARN]-+===OD TE VRSTICE NAPREJ JE DNEVNIŠKA DATOTEKA V PODMAPI TEGA SISTEMA!!!===+');
      AddToLog('[APP-EXEC]-Poizkušam premakniti podmapo "vars" in njeno vsebino...');
      fpsystem('cp -ar /proins/vars/* ' + RunDir + '/vars');
      AddToLog('[APP-OK]-Podmapa "vars" in njena vsebina uspešno premaknjena.');
      AddToLog('[APP-EXEC]-Poizkušam premakniti podmapo "binaries" in njeno vsebino...');
      fpsystem('cp -ar /proins/binaries ' + RunDir + '/binaries');
      AddToLog('[APP-OK]-Podmapa "binaries" in njena vsebina uspešno premaknjena.');
      AddToLog('[APP-EXEC]-Poizkušam premakniti podmapo s skriptami...');
      fpsystem('cp -ar /proins/training-wheels ' + RunDir + '/scripts');
      AddToLog('[APP-OK]-Podmapa z skriptami uspešno premaknjena.');
      AddToLog('[APP-EXEC]-Generiram datoteko s časovnim žigom prvega zagona zaganjalnika...');
      fpsystem('echo ' + (FormatDateTime('DD. MM. YYYY - hh:mm:ss', now)) + ' > ' + RunDir + '/vars/installer_first_start.log');
      AddToLog('[APP-OK]-Datoteka s časovnim žigom prvega zagona zaganjalnika uspešno generirana.');
      //Removing clumsilly parented "disk" directory whitin /vars/ working folder.
      fpsystem('rm -rf ' + RunDir + '/vars/' + DiskFolder + '/');
      //Finally we inform the status handler with correct value.
      WorkingStatus := 0;
   end;
   //If all went well (no error messeages), this one-time-called procedure should suffice trough whole session of program.
end;


//Procedure for checking the actual state, in which installer runs. If parameters were allready set, then it disables
//controls in upper part of program (enabled by default), and leaves set parameters in assigned controls and automaticly
//calls for MainInstaller method to continiue the work since last the restart (or other interruption).
//The procedure also enables the "lower" portion of the window, so user can monitor the progress of instalation.
procedure CheckProgressState();
var
   DumbArray: TStringList;
begin
   If (FileExists(RunDir + '/vars/user_selected_params.list')) then
   begin
       DumbArray := TStringList.Create();
       DumbArray.LoadFromFile(RunDir + '/vars/user_selected_params.list');
       Form1.ComboBox1.Text := DumbArray.Strings[0];
       Form1.ComboBox2.Text := DumbArray.Strings[1];
       DumbArray.Free();
       Form1.GroupBox1.Enabled := False;
       Form1.GroupBox2.Enabled := False;
       Form1.ComboBox1.Enabled := False;
       Form1.ComboBox2.Enabled := False;
       Form1.BitBtn1.Enabled := False;
       //Upper code is for the "upper" portion of the main window. Below we take care for the "lower" portion.
       Form1.GroupBox3.Enabled := True;
       Form1.GroupBox4.Enabled := True;
       Form1.Label1.Enabled := True;
       Form1.Label2.Enabled := True;
       Form1.ProgressBar1.Enabled := True;
       Form1.ProgressBar2.Enabled := True;
       Application.ProcessMessages();
       GetRunningState();
   end;
end;


//This procedure is a overwritten procedure of Object Pascal's component "PopupNotifier",
//since it gave us quite a headache while trying to develop a simple and
//discrete method to show progress of actions to user. Linker problems,
//time-outs, eventualy developer threw everything out and rewrote whole damn
//thing using a single no-frame form and couple of static text strings.
//For a added art phenomena, AlphaBlending is included to give it more "pop-up" look.
//And yes, you can incorporate it into your own project, because whole damn thing is open-sourced.
procedure ShowInfoBaloon(BaloonName : String ; BaloonText : String);
begin
   //There still may be some residual text in previous baloon instance, so
   //it has to be cleaned.
   Form7.Label2.Caption := '';
   Form7.Label3.Caption := '';
   //The new test i.e. the new static text strings are priped trough calling
   //this procedure with two known variables.
   Form7.Label2.Caption := BaloonName;
   Form7.Label3.Caption := BaloonText;
   //First we make sure that the "baloon" is not visible.
   Form7.Show;
   AddToLog('[APP-EXEC]-Uspešno prikazan "InfoBaloon".');
   //Finaly we make it gradialy visible, until AlphaBlend tops off at factor 255
end;


//At each run this procedure checks for first instance (first start) of this
//application and generates necesary files for correct step handling.
procedure CheckFirstStart();
var
   CurrentDateTime : String;
begin
     If (WorkingStatus = 0) then
     begin
        //Informing user that this has been the first start of application.
        //The "InfoBaloon" is triggered by Timer1.
        //Updating file for consequent run of application.
        fpsystem('touch ' + RunDir + '/vars/installer_first_start.log');
        CurrentDateTime := FormatDateTime('DD. MM. YYYY - hh:mm:ss', now);
        fpsystem('echo ' + CurrentDateTime + ' > ' + RunDir + '/vars/installer_first_start.log');
        AddToLog('[APP-INFO]-Uspešno dodan časovni žig v datoteko installer_first_start.log.');
     end;
end;


//At each run this procedure checks for last succesfull instance (last start)
//of this application and generates correct time-stamp for correct step handling.
procedure CheckLastStart();
var
   CurrentDateTime : String;
   DateFileReadArray: TStringList;
begin
   If (WorkingStatus = 1) then
       begin
          AddToLog('[APP-EXEC]-Ugotovljen vnovični zagon namestitvenega programa.');
          //In case of first run, the "last_start" file must also be generated and
          //time-stamp is created and stored for future initialisations of the program.
          If FileExists(RunDir + '/vars/installer_last_start.log') then
          begin
             DateFileReadArray := TStringList.Create();
             DateFileReadArray.LoadFromFile(RunDir + '/vars/installer_last_start.log');
             LastDateTime := DateFileReadArray.Strings[0];
             AddToLog('[APP-INFO]-To je ponovni zagon namestilnika po ' + LastDateTime + ' Nadaljujemo z namestitvijo.');
             CurrentDateTime := FormatDateTime('DD. MM. YYYY - hh:mm:ss', now);
             fpsystem('echo ' + CurrentDateTime + ' > ' + RunDir + '/vars/installer_last_start.log');
             AddToLog('[APP-INFO]-Uspešno dodan časovni žig v datoteko installer_last_start.log.');
             DateFileReadArray.Free();
          end;
     end;
end;


//Using bash tool "lsblk" information about local block devices is piped through
//string overload method and stored in local plain-text file. The same file
//serves as imput to another method, which filters only acceptable choices and
//lists them in user selection window. All iterations of "lsblk" are run in
//background, so no user interference is predicted.
procedure GetLocalDiskDriveInformation();
var
   MemoLines : integer;
   Step : integer;
begin
   //Running bash commands must be invoked with "-c" (concealed) switch to prevent "screen-bloating".
   //Output of instance of a bash command is routed to "S" ansi-string variable, globally accessible.
   Application.ProcessMessages();
   RunCommand('/bin/bash',['-c','lsblk -o NAME,SIZE,FSTYPE,STATE,ROTA,VENDOR,MODEL,SERIAL > ' + RunDir + '/vars/local_available_disks.list'], s);
   Application.ProcessMessages();
   Form1.ComboBox1.Items.Clear;
   Form2.Memo1.Lines.LoadFromFile(RunDir + '/vars/local_available_disks.list');
   MemoLines := Form2.Memo1.Lines.Count;
   Step := 1;
   while (Step < MemoLines) do
   begin
      If not ((Form2.Memo1.Lines.Strings[Step].Contains('loop')) or
              (Form2.Memo1.Lines.Strings[Step].Contains('├')) or
              (Form2.Memo1.Lines.Strings[Step].Contains('└'))) then
      begin
        //When all non-viable choices are removed, the rest is sorted into ComboBox
        //graphical element for user to choose. Elements are arranged alphabetically.
        Form1.ComboBox1.Items.Add('/dev/' + LeftStr(Form2.Memo1.Lines.Strings[Step],5));
      end;
      Step := Step + 1;
   end;
   AddToLog('[APP-OK]-Seznam blokovnih naprav je bil uspešno osvežen.');

end;


//Using bach tool "lsblk" to show just attached blok devices in more discrete manner,
//so users don't neccesary get entangled with all the unneccecary details of output.
//This procedure derives it's operation strictly in alocated memory and has no
//physical disk I/O operations.
procedure ListLocalDiskDrives();
var
   Operation : TProcess;
   ListLines : TStringList;
   LinePosition : Integer;
   AllLines : Integer;
begin
   LinePosition := 0;
   Operation := TProcess.Create(nil);
   ListLines := TStringList.Create;
   Operation.Options := Operation.Options + [poWaitOnExit, poUsePipes];
   Operation.CommandLine := 'lshw -short -quiet -c disk';
   Operation.Execute();
   Application.ProcessMessages();
   ListLines.LoadFromStream(Operation.Output);
   AllLines := ListLines.Count;
   Form2.Memo1.Lines.Clear();
   repeat
      Form2.Memo1.Lines.Add(ListLines.Strings[LinePosition]);
      Inc(LinePosition);
   until (LinePosition = AllLines);
   Application.ProcessMessages();
   Operation.Free();
   ListLines.Free();
   Form2.Show();
end;


//This procedure runs basic S.M.A.R.T. information read-out on selected hard disk drive
//Result of smartctl is in temporary allocated memory for short period of time, so window
//with information dialog can be created. Window for showing this information is being "shared"
//with multiple functions and procedures to save binary size and execution cylces.
procedure GetSMARTDataFromDrive();
var
   Operation : TProcess;
   ListLines : TStringList;
   LinePosition : Integer;
   AllLines : Integer;
   TargetDisk : String;
begin
   if (Form1.ComboBox1.ItemIndex = -1) then
   begin
      Application.MessageBox('Prosimo, izberite disk, na katerega nameravate namestiti sistem, da ga lahko preverimo!', 'Nepravilna izbira', MB_ICONHAND);
   end;
   if not (Form1.ComboBox1.ItemIndex = -1) then
   begin
      TargetDisk := Form1.ComboBox1.Text;
      Application.ProcessMessages();
      LinePosition := 4;
      Operation := TProcess.Create(nil);
      ListLines := TStringList.Create;
      Operation.Options := Operation.Options + [poWaitOnExit, poUsePipes];
      Operation.CommandLine := ('smartctl -a ' + TargetDisk);
      Operation.Execute();
      Application.ProcessMessages();
      ListLines.LoadFromStream(Operation.Output);
      AllLines := ListLines.Count;
      Form6.Memo1.Lines.Clear();
      AddToLog('[APP-EXEC]-Namestilnik pripravlja izpis SMART za pogon ' + TargetDisk + '.');
      Form6.Memo1.Lines.Add('|> > > > > > > > > > > O P O Z O R I L O ! ! ! < < < < < < < < < < <   ');
      Form6.Memo1.Lines.Add(' Izpis v nadaljevanju je "grobi", nefiltrirani izpis ukaza SmartCtrl,  ');
      Form6.Memo1.Lines.Add(' ki lahko (glede na uporabniško izkušenost) pove uporabniku veliko     ');
      Form6.Memo1.Lines.Add(' (ali pa popolnoma ničesar) o tem, v kakšnem stanju je disk, ki ga     ');
      Form6.Memo1.Lines.Add(' imamo pod "drobnogledom". Če nisi prepričan-a, kaj pomenijo izpisani  ');
      Form6.Memo1.Lines.Add(' podatki in ali je ta disk sploh primeren za nadaljevanje instalacije  ');
      Form6.Memo1.Lines.Add(' predlagamo, da se o tem z nekom posvetuješ. Vsem bo veliko lažje. :)  ');
      Form6.Memo1.Lines.Add('=======================================================================');
      Form6.Memo1.Lines.Add('');
      Form6.Memo1.Lines.Add('');
      repeat
         Form6.Memo1.Lines.Add(ListLines.Strings[LinePosition]);
         Inc(LinePosition);
      until (LinePosition = AllLines);
      Application.ProcessMessages();
      Operation.Free();
      ListLines.Free();
      Form6.Caption := 'Osnovni S.M.A.R.T. izpis za pogon ' + TargetDisk;
      AddToLog('[APP-OK]-Osnovno poročilo S.M.A.R.T. za pogon ' + TargetDisk + 'je pripravljeno, Odpiram prikaz uporabniku.');
      Form6.Show();
   end;
end;


//Wonderfull function below this comment line block is an "old-school" piece of
//code for "catching" running processes. It uses a single-string variable
//as a name of process, which it "hunts" for. If it finds it in the memory pool
//(assuming the process is running under it's original name) it gives a "thumbs-up".
//The most usefull feature of this function is its simplicity, results are instant
//ant the function itself does not interfere with processes execution in any way.
function IsScriptRunning(ScriptName:String):Boolean;
var
   Script : TProcess;      //We will create a dummy process in this function thus
   Readout : TStringList;  //we have to declare a function-wide object-variable for it.
begin
   Result := False;  //Result will allways be false, unless the process is identified.
   Script := TProcess.Create(nil);  //Using a "dummy" process to "hunt for" a "real" one.
   Script.CommandLine:='ps -C' + ScriptName;  //Asigning it a name (a trace name).
   Script.Options:=[poUsePipes,poWaitonexit]; //Walking trough memory pool of running processes
   try   //We are after all pricking with the system memory, so we want to avoid any sudden crashes, must use try-catch-finally block.
      Script.Execute;   //We run the prepared "dummy" with trace name.
      Readout := TStringList.Create;   //And we "catch" its output, generaly on some /dev/ttyX, but not important, since we pipe it back here.
      try
         Readout.LoadFromStream(Script.Output);    //Once the output has been "caught"
         Result:=Pos(ScriptName,Readout.Text)>0;   //we compare it to our original name-in-question and voilà! :)
      finally
         Readout.free;  //Of course, nice and tidy, let's not trash the memory.
      end;
    finally
       Script.Free;  //Since there is no need for the dummy process, we let it go, too.
    end;
end;


//Invoking pre-load of time-stamps, stored in two seperate log files.
//Time-stamps are recpectivly casted to a string format and displayed in
//time statistics window upon user interaction
procedure GetInstallerRunStatistics();
var
   DateFileReadArray: TStringList;
begin
   If FileExists(RunDir + '/vars/installer_first_start.log') then
     begin
       Form3.Label7.Caption := '';
       DateFileReadArray := TStringList.Create();
       DateFileReadArray.LoadFromFile(RunDir + '/vars/installer_first_start.log');
       Form3.Label7.Caption := DateFileReadArray.Strings[0];
       DateFileReadArray.Free();
     end;
     //We assume, that this file allready exists and handle it properly.
     //If installer is ran for the first time, seperate funcion creates the
     //necesary file, using "touch" method via bash interpreter.
     If FileExists(RunDir + '/vars/installer_last_start.log') then
     begin
       Form3.Label8.Caption := '';
       DateFileReadArray := TStringList.Create();
       DateFileReadArray.LoadFromFile(RunDir + '/vars/installer_last_start.log');
       Form3.Label8.Caption := DateFileReadArray.Strings[0];
       DateFileReadArray.Free();
     end;
     AddToLog('[APP-INFO]-Aplikacija je uspešno pripravila časovne statistike zagonov programa.');
     AddToLog('[USER-INFO]-Uporabnik je preveril časovne statistike programa za namestitev.');
     Form3.ShowModal();
end;


//Second mandatory parameter is version of system to be progressivly installed
//onto local system. All available versions are listed by their respective
//directory name and stored in pre-defined plain-text file, subsequently
//string-casted into ComboBox graphical element for user to choose.
procedure GetAvailableInstalationsList();
begin
   Form1.ComboBox2.Items.Clear;
   //All available instalations and respective directories are stored in static
   //plain-text file, which is loaded into graphical portion of user-select menu.
   Form1.ComboBox2.Items.LoadFromFile(RunDir + '/vars/nfs.txt');
   AddToLog('[APP-OK]-Nabor instalacij, ki so na voljo je bil osvežen v seznamu.');
end;


//Throughout application runtime a rigorous logging takes place. In case of
//malfunction, error or pin-pointing a bug, the plain-text log file is
//loaded in reverse order (last entry on top of list) in a viewer window for
//user to quickly and simply examine the contents and rectify any known problems.
procedure LoadRunLog();
var
   Concatenator : TStringList;
   LinesCount : Integer;
begin
   //By default, we asume we have read access of a log file, while the log file is
   //is in use and hence locked. Using read-only file handling prevents segmentation
   //and access violation problems.
   Concatenator := TStringList.Create();
   AddToLog('[APP-LOAD]-Namestilnik pripravlja dnevniško datoteko za prikaz v oknu pregledovalnika.');
   Concatenator.LoadFromFile(RunDir + '/proins_runlog.log');
   Form5.Memo1.Lines.Clear();
   AddToLog('[APP-OK]-Uporabniku je prikazano okno za pregled dnevnika izvajanja namestilnika.');
   LinesCount := Concatenator.Count;
   //For debug purposes only. Counts number of lines in the log file and shows it in dialog.
   //ShowMessage(IntToStr(LinesCount));
   repeat
     //Since TMemo component is listing indexes starting with 1 (others normally with 0),
     //loading via index count must be done extremely carefully!
     Form5.Memo1.Lines.Add(Concatenator.Strings[(LinesCount - 1)]);
     Dec(LinesCount);
   until (LinesCount = 0);
   //To avoid memory-leaks all file-handling methods must be destroyed upon handling.
   //All viable data must be secured (stored) prior to freeing allocated memory.
   Concatenator.Free();
   Form5.ShowModal();
end;


//Improved procedure for checking user input coherence and relaying user
//selection to the next stage of installer. Most of the graphic code was left,
//however some new slick logick has been applied.
procedure ShowConfirmationWindow();
begin
   //If there was no selection in a drop-down menu, the user is warned about it.
   if (Form1.ComboBox1.ItemIndex = -1) then
   begin
     Application.MessageBox('Prosimo, izberite ciljni disk, na katerega se bo namestil nov sistem!', 'Manjkajoč vnos parametra', MB_ICONHAND);
     //For aditional visual assistance, key errata element in question changes colour, when information is missing.
     Form1.ComboBox1.Color := clHighlight;
     AddToLog('[USER-ERROR]-Uporabnik pozabil nastaviti izbiro ciljnega diska.');
   end;
   if (Form1.ComboBox2.ItemIndex = -1) then
   begin
     Application.MessageBox('Prosimo, izberite končni sistem, ki ga bomo kopirali na ciljni disk!', 'Manjkajoč vnos parametra', MB_ICONHAND);
     Form1.ComboBox2.Color := clHighlight;
     AddToLog('[USER-ERROR]-Uporabnik pozabil nastaviti izbiro ciljnega sistema.');
   end;

   //If all parameters have been set correctly, information is passed on to final user check window
   //for a couple of more questions - just to be sure about this installation.
   if not ((Form1.ComboBox1.ItemIndex = -1) or (Form1.ComboBox2.ItemIndex = -1)) then
   begin
      Form4.TargetDir := RunDir;
      Form4.StaticText2.Caption := ('Izbrali ste sistem ' + Form1.ComboBox2.Items[Form1.ComboBox2.ItemIndex] + ' ki bo nameščen na disku ');
      Form4.StaticText2.Caption := (Form4.StaticText2.Caption + Form1.ComboBox1.Items[Form1.ComboBox1.ItemIndex] + ',ki bo pred tem izbrisan.');
      AddToLog('[USER-OK]-Uporabnik nastavil željene parametre, nadaljujem z avtentikacijo parametrov.');
      Form4.Show();
   end;
end;


//In case of logical or abstract-level "hic-up" in application and/or included scripts
//this procedure serves as a "hard-reset" feature. It is a one-push master reset switch,
//which moves (not just deletes) all the critical files and restarts the application in
//single execution. After the operation-critical files have been removed, and entire
//application is restarted, whole procedure starts over, as if it was the intaller's first run.
procedure ResetInstallProcedure();
var
   BoxStyle : Integer;
   Responce : Integer;
   DateTimeStamp : String;
begin
   //Since this is a critical step, user must confirm his choice.
   AddToLog('[USER-INFO]-Uporabnik zahteva ponastavitev celotnega postopka, čakam na odobritev.');
   BoxStyle := MB_ICONQUESTION + MB_YESNO;
   Responce := Application.MessageBox('Ali ste prepričani, da želite preklicati vse spremembe in začeti postopek nameščanja znova?', 'Ponastavitev celotnega postopka namestilnika', BoxStyle);
   if Responce = IDYES then
   begin
      ShowMessage(RunDir);
      //We add note to log for later diagnosing of any problems.
      AddToLog('[APP-INFO]-Ponastavitev odobrena, delovni direktorij preimenovan, zaustavljam aplikacijo.');
      Application.ProcessMessages();
      DateTimeStamp := FormatDateTime('DD-MM-YYYY--hh-mm-ss', now);
      //Objective is acomplished basicly by renaming current directory by adding timestamp suffix to directory name.
      fpsystem('mv ' + RunDir + ' ' + RunDir + '_OLDRUN_' + DateTimeStamp);
      //Syncing filesystem at this point is probably overkill, but just to be sure.
      fpsystem('sync');
      Application.ProcessMessages();
      //If procedure was succesfull, we inform the user and stop the application.
      Application.MessageBox('Postopek ponastavitve je uspešno zaključen. Namestilnik se bo sedaj zaustavil in ga lahko zatem ročno ponovno zaženete.', 'Zaustavitev in ponovni zagon namestilnika', MB_ICONHAND);
      Form1.Close();
      Form1.Free();
      //Let's not forget about tidying up
      Application.Terminate();
   end
      else
      begin
      Application.MessageBox('Ponastavitev procesa namestilnika je bila preklicana', 'Preklic ponastavitve celotnega procesa', MB_ICONHAND);
      //Result of user choice is logged in either case.
      AddToLog('[USER-INFO]-Uporabnik preklical dokončno avtorizacijo ponastavitve postopka namestilnika. Svet se vrti naprej.');
    end;
end;


//This is just a future set-up-point for script flow and control handling.
//Some of the big things are about to happen here.
procedure InstallerRun();
begin
   //Calling this procedure just for testing

end;



{-------------------THIS IS THE RUBICON LINE, DON'T LOOK BACK!------------------}
//After this line, all the procedures are machine (software) invoked.

//Initial void procedure, invoked at the initialisation of this app. Will hold
//important procedures of code for proper step and instruction handling in future.

procedure TForm1.FormCreate(Sender: TObject);
begin
   //Uppon main-form is created and shown, a specific sequence sets in progress,
   //ensuring all information is properly selected and stored for processing.
   //First of all, aplication startup is being logged, to save the timestamp.
   AddToLog('[APP-INFO]]-Uspešen zagon modulov aplikacije, prikazujem glavno okno.');
   //Localised procedure checks if this is a first time installer has been run.
   SetWorkingDirectory();
   Application.ProcessMessages;
   CheckProgressState();
   CheckFirstStart();
   //This localised procedure checks the last time installer was run and sets timestamps
   //approprieatley. It is these two procedures which are in charhe of changing of main window's "appearance".
   CheckLastStart();
   //Selection of available NFS installations is loaded into second drop-down checkbox
   //A "ShowInfoBaloon" procedure is invoked after a precise amount of time to show information
   //about first or last run of this installation.
   GetAvailableInstalationsList();
   //GetLocalDiskDriveInformation(); //----> This funcion is being invoked 0,5 seconds after the
   //initial application startup by Timer1 trigger. This is mandatory to avoid stack related errors.
   Timer1.Enabled := True;
end;



//User graphical window for showing installer time statistics.
//This procedure only calls another function, which handles window spawn
//and object registration.
procedure TForm1.MenuItem2Click(Sender: TObject);
begin
   GetInstallerRunStatistics();
   AddToLog('[APP-OK]-Pripravljen časovno-statistični izpis pregleda izvedenih dogodkov v aplikaciji.');
end;


//User graphical window for showing available disk drives an respective details.
//This procedure only calls another function, which handles window spawn
//and object registration.

procedure TForm1.MenuItem5Click(Sender: TObject);
begin
   ListLocalDiskDrives();
   AddToLog('[USER-INFO]-Uporabnik je preveril seznam priključenih blokovnih naprav v ciljnem računalniku.');
end;

procedure TForm1.MenuItem6Click(Sender: TObject);
begin
  ResetInstallProcedure();
end;

procedure TForm1.MenuItem8Click(Sender: TObject);
begin
   GetSMARTDataFromDrive();
end;


//Procedures for showinf InfoBaloons on application start-up. On each instance of start-up
//application finds fragmemts of previous runs and shows message acordingly.
//It the installer is being run for the first time only, set of code changes the directory
//structure while another message is displayed to the user.
//SHOWING INFO-BALLOONS became a tricky business in this project, since there is
//absolutley no GTX+ support for a module like this and we had to nail it all on
//our own. Not the prettiest bunch, but at least it works the way it should work.
//And the color is the one we like. Finally.

procedure TForm1.Timer1Timer(Sender: TObject);
var
   SecondRunString : String;
begin

   AddToLog('[APP-OK]-Sporočilni del glede zagona namestilnika uspešno izveden in zaključen.');

   AddToLog('[APP-ERROR]-Ne morem generirati seznama diskov, ki so na voljo za instalacijo, poizkušam še enkrat...');
   //For security reasons, available disk file is generated about two seconds after the main application starts
   GetLocalDiskDriveInformation();
   AddToLog('[APP-OK]-Seznam diskov, ki so na voljo za instalacijo, je bil uspešno ponovno generiran, lahko nadaljujemo...');
   SecondRunString := ('To je ponovni zagon namestilnika po ' + LastDateTime + '. Namestitev se nadaljuje z nastavljenimi parametri.');
   If (WorkingStatus = 0) then
   begin
      ShowInfoBaloon('Prvi zagon OEM namestitve','Kaže, da je to prvi zagon namestitvenega programa. Prosimo, izberite parametre!');
   end;
   If (WorkingStatus = 1) then
   begin
      ShowInfoBaloon('Vnovični zagon OEM namestitve',SecondRunString);
   end;
   Timer1.Enabled := False;
end;


//User graphical window for showing application (installer) run-time log.
//This procedure only calls another function, which handles window spawn
//and object registration.

procedure TForm1.MenuItem3Click(Sender: TObject);
begin
   //Log contents are casted in read-only mode, so application can perform logging
   //while the log is being viewed. Mind that log is not refreshed during viewing.
   AddToLog('[APP-INFO]-Uporabnik zahteval pripravo prikaza dnevnika izvajanja namestilnika.');
   LoadRunLog();
end;

//While most of variables are static (one-time setting), choice of target block
//device can be set several times over and over during pre-installation parameter
//setting. Since this parameter is vital for correct step handling and hence for
//succesfull installation procedure, the change is noted and updated globally
//troughout application, each time user changes it (or reverts allready set choice)
//in the graphical portion of this application procedure

procedure TForm1.ComboBox1Change(Sender: TObject);
var
   UserItemSelected : integer;
begin
   UserItemSelected := ComboBox1.ItemIndex;
   //Storing last choice each time, change trigger occurs.
   UserDiskSelected := ComboBox1.Items[UserItemSelected];
   //Each time new variable is stored globally.
   AddToLog('[APP-OK]-Uporabnik je za ciljni disk izbral napravo "' + UserDiskSelected + '".');
end;


//User-driven procedure for starting the installation procedure. When all
//parameters are (correct) and set, procedure invokes final confirmation of
//procedure by calling upon pre-defined set of confirmation dialogs.

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
   //If all information is in the correct place, a confirm window can be called upon.
   ShowConfirmationWindow();
end;

end.



{+-----------------------------------------------------------------------------+}
{|                           *** END OF FILE ***                               |}
{+-----------------------------------------------------------------------------+}

