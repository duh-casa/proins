#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)

touch ${vars}/status/09-grow-fs.begin

resize2fs ${disk}1
touch /forcefsck
sync

touch ${vars}/status/09-grow-fs.end

