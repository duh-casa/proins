#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)

touch ${vars}/status/02-partition.begin

parted -s -a optimal -- ${disk} mklabel msdos
parted -s -a optimal -- ${disk} mkpart primary ext4 '0%' '10GB'
parted -s -a optimal -- ${disk} mkpart primary free '10GB' '100%'

partprobe

dd if=/dev/zero of=${disk}1 bs=512 count=1024
dd if=/dev/zero of=${disk}2 bs=512 count=1024

mkfs.ext4 ${disk}1

partprobe

touch ${vars}/status/02-partition.end

