{+-----------------------------------------------------------------------------+}
{| RSO Progressive installer - Unit2 / Form2, version 0.7                      |}
{| (c) Vassugo and Dustwolf, 2020                                              |}
{|                                                                             |}
{| Graphical front-end, application window for listing block disk devices      |}
{| locally present in the computer. Uses native "lsblk" tool to get results.   |}
{+-----------------------------------------------------------------------------+}

unit Unit2;

{$mode objfpc}{$H+}

interface

// Important: Overload calls must be implemented in each unit - for each used unit
uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons;

type

  { TForm2 }

  TForm2 = class(TForm)
    BitBtn1: TBitBtn;
    Memo1: TMemo;
    procedure BitBtn1Click(Sender: TObject);
  private

  public

  end;

var
  Form2: TForm2;

implementation

{$R *.lfm}

{ TForm2 }


//Since there is no code to drive this section of application, only procedure
//implemented in this form is window-close method. Since this window is used
//more than one time in application runtime, no destructor is implemented.

procedure TForm2.BitBtn1Click(Sender: TObject);
begin
  Form2.Close();
end;

end.


{+-----------------------------------------------------------------------------+}
{|                           *** END OF FILE ***                               |}
{+-----------------------------------------------------------------------------+}

