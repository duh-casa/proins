#!/bin/bash
if [ "$1" != "authtoken" ]; then
  echo "Invalid auth"
  exit 1
fi
vars=../$(../GetDiskID/GetDiskID $2)/vars
mkdir -p ${vars}/status
disk=$(cat ${vars}/disk.txt)

touch ${vars}/status/08-swap.begin

e2label ${disk}2 swap
mkswap ${disk}2
sswap -f -l -l -z ${disk}2
swapon ${disk}2

echo "${disk}2      none            swap    sw              0       0">>/etc/fstab

touch ${vars}/status/08-swap.end

